"""SIMBION20 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import halaman_utama.urls as halaman_utama
import login.urls as login
import register.urls as register
import daftarbeasiswa.urls as daftarbeasiswa


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(halaman_utama, namespace='halaman_utama')),
    url(r'^login/', include(login, namespace='login')),
    url(r'^register/', include(register, namespace='register')),
    url(r'^daftarbeasiswa/', include(daftarbeasiswa, namespace='daftarbeasiswa')),
]
