from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    #url(r'^$', login, name='login'),
    url(r'^pendaftaranpaket/$', pendaftaranpaket, name='pendaftaranpaket'),
    url(r'^tambahbeasiswa/$', tambahbeasiswa, name='tambahbeasiswa'),
    url(r'^registrasi_paket/$', registrasi_paket, name='registrasi_paket'),
    url(r'^registrasi_beasiswa/$', registrasi_beasiswa, name='registrasi_beasiswa'),
    #url(r'^auth_logout/$', auth_logout, name='auth_logout'),
]