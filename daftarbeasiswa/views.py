from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from halaman_utama.models import *
from halaman_utama.views import dictfetchall

cursor = connection.cursor()

# Create your views here.
def pendaftaranpaket(request):
    response = {}
    return render(request, 'daftarbeasiswa/pendaftaranpaket.html', response)

def tambahbeasiswa(request):
    response = {}
    skemabeasiswa = SkemaBeasiswa.objects.raw('SELECT * FROM skema_beasiswa')
    response["skemabeasiswa"] = skemabeasiswa
    return render(request, 'daftarbeasiswa/tambahbeasiswa.html', response)

def registrasi_paket(request):
	if request.method == "POST":
		kode = request.POST['kode']
		nama_paket = request.POST['nama_paket']
		jenis_paket = request.POST['jenis_paket']
		deskripsi = request.POST['deskripsi']
		syarat_beasiswa = request.POST['syarat_beasiswa']
		cursor.execute('''	INSERT INTO SKEMA_BEASISWA(kode, nama, jenis, deskripsi, nomor_identitas_donatur) 
							VALUES ( %s, %s, %s, %s, '273-53-3379');''', [kode, nama_paket, jenis_paket, deskripsi])
		response = {}
		return HttpResponseRedirect(reverse('halaman_utama:index'))

def registrasi_beasiswa(request):
	if request.method == "POST":
		kodebeasiswa = request.POST['kodebeasiswa']
		no_urut = request.POST['no_urut']
		tanggal_mulai = request.POST['tanggal_mulai']
		tanggal_selesai = request.POST['tanggal_selesai']
		cursor.execute('''	INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, jumlah_pendaftar, total_pembayaran) 
							VALUES ( '%s', %s, %s, %s, 'Sekarang', 'Buka', 0, 0);''', [int(kodebeasiswa), no_urut, tanggal_mulai, tanggal_selesai])
		response = {}
		return HttpResponseRedirect(reverse('halaman_utama:index'))