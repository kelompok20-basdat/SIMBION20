from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.forms.models import model_to_dict
from .models import Pengumuman, SkemaBeasiswaAktif, SyaratBeasiswa
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import connection

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
# Create your views here.
response = {}

def index(request):
    print ("#==> masuk index")

    # Untuk masukin pengumuman ke halaman utama
    pengumuman = Pengumuman.objects.raw('SELECT * FROM pengumuman ORDER BY tanggal DESC')
    dict_pengumuman = []
    ii = 0
    for p in pengumuman:
        dictio = model_to_dict(p)
        dictio["id"] = ii
        dict_pengumuman.append(dictio)
        ii += 1
    pengumuman_list_for_pagination = dict_pengumuman
    paginator = Paginator(pengumuman_list_for_pagination, 2)
    page = request.GET.get('pengumuman_page')
    try:
        all_pengumuman = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        all_pengumuman = paginator.page(1)
    response["all_pengumuman"] = all_pengumuman

    # Untuk masukin beasiswa aktif ke halaman utama
    get_beasiswa_aktif = connection.cursor()
    get_beasiswa_aktif.execute('''      SELECT * 
                                        FROM skema_beasiswa_aktif, skema_beasiswa 
                                        WHERE kode_skema_beasiswa = kode
                                        ORDER BY tgl_tutup_pendaftaran ASC''')
    dict_beasiswa_aktif = dictfetchall(get_beasiswa_aktif)
    print(dict_beasiswa_aktif)
    jj = 1
    for p in dict_beasiswa_aktif:
        p["id"] = jj
        jj += 1
    beasiswa_aktif_list_for_pagination = dict_beasiswa_aktif
    paginator = Paginator(beasiswa_aktif_list_for_pagination, 3)
    page = request.GET.get('beasiswa_aktif_page')
    try:
        all_beasiswa_aktif = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        all_beasiswa_aktif = paginator.page(1)
    response["all_beasiswa_aktif"] = all_beasiswa_aktif

    # define html yang mau di render
    html = 'halaman_utama/halaman_utama.html'

    # check ada yang login atau enggak
    if 'user_login' in request.session:
        response['is_login'] = True
        response['role'] = request.session['role']
        print(response['role'])
        return render(request, html, response)

    else:
        response['is_login'] = False
        response['role'] = ""
        return render(request, html, response)

    