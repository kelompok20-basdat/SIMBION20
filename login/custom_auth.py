from django.contrib import messages
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core import serializers
from halaman_utama.models import Pengguna, Mahasiswa, Donatur, Admin
import json
from django.forms.models import model_to_dict


#authentication
def auth_login(request):
    print ("#==> auth_login ")
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        try:
            person = Pengguna.objects.raw('SELECT * FROM pengguna WHERE username = %s', [username])[0]
            print(person.username)
            try:
                person = Pengguna.objects.raw('SELECT * FROM pengguna WHERE username = %s AND password = %s', [username, password])[0]
                request.session['role'] = person.role
                if person.role == "mahasiswa":
                    mahasiswa = Mahasiswa.objects.raw('SELECT * FROM mahasiswa WHERE username = %s', [person.username])[0]
                    request.session['user_login'] = mahasiswa.username.username
                    request.session['npm'] = mahasiswa.npm
                    return HttpResponseRedirect(reverse('halaman_utama:index'))
                elif person.role == 'admin':
                    admin = Admin.objects.raw('SELECT * FROM admin WHERE username = %s', [person.username])[0]
                    request.session['user_login'] = admin.username.username
                    return HttpResponseRedirect(reverse('halaman_utama:index'))
                elif person.role == 'donatur':
                    donatur = Donatur.objects.raw('SELECT * FROM donatur WHERE username = %s', [person.username])[0]
                    request.session['user_login'] = donatur.username.username
                    request.session['nomor_identitas'] = donatur.nomor_identitas
                    return HttpResponseRedirect(reverse('halaman_utama:index'))
                else :
                    return HttpResponseRedirect(reverse('login:login'))
            except Exception as e:
                response = {}
                messages.error(request, "Username atau password salah")
                response['is_login'] = False
                response['wrong_password'] = True
                html = 'login/login.html'
                return render(request, html, response)   
        except Exception as e:
            response = {}
            messages.error(request, "Username atau password salah")
            response['is_login'] = False
            response['wrong_account'] = True
            html = 'login/login.html'
            return render(request, html, response)        
    #     #call csui_helper
    #     try:
    #         kode_identitas = ver_user['identity_number']
    #         role = ver_user['role']

    #         # set session
    #         request.session['user_login'] = username
    #         request.session['access_token'] = access_token
    #         request.session['kode_identitas'] = kode_identitas
    #         request.session['role'] = role
    #         user, is_login = User.objects.get_or_create(identity_number=kode_identitas, name=username)
    #         messages.success(request, "Anda berhasil login")
    #     except Exception as e:
    #         messages.error(request, "Username atau password salah")
    #         return HttpResponseRedirect(reverse('login-status:login'))
    # return HttpResponseRedirect(reverse('login-status:status'))

def auth_logout(request):
    print ("#==> auth logout")
    request.session.flush() # menghapus semua session
    
    messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
    print("Anda berhasil logout. Semua session Anda sudah dihapus")
    return HttpResponseRedirect(reverse('halaman_utama:index'))
