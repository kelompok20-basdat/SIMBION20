from django.conf.urls import url
from .views import login
from .custom_auth import auth_login, auth_logout
#url for app
urlpatterns = [
    url(r'^$', login, name='login'),
    url(r'^auth_login/$', auth_login, name='auth_login'),
    url(r'^auth_logout/$', auth_logout, name='auth_logout'),
]