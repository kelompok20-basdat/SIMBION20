from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

# Create your views here.
response = {}
def login(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        response['is_login'] = True
        return HttpResponseRedirect(reverse('halaman_utama:index'))
    else:
        response['is_login'] = False
        html = 'login/login.html'
        return render(request, html, response)