# THIS REPOSITORY ARE USED FOR DATA BASE PROJECT

# Guide To Run This Project Locally

1.	You must have at least Python 3.5

2.	Create new environtment inside this folder (Run in cmd inside this folder: python -m venv env )

3. 	install everythings in the requirements.txt inside the environtment that you've just create (Run in cmd inside this folder in the new environtment: pip install -r requirements.txt)

4.	Run the project!!! (inside your environtment in cmd run: python manage.py runserver)

5.	In your web browser go to link https://localhost:8000/)

6.	Done!!!!

# Note: You must at least have postgreSQL with related schema locally to so this app can run smoothly with the settings in the project setting