CREATE SCHEMA SIMBION;

SET SEARCH_PATH TO SIMBION;

CREATE TABLE PENGGUNA (
	username VARCHAR(20) PRIMARY KEY,
	password VARCHAR(20) NOT NULL,
	role VARCHAR(20) NOT NULL
);

CREATE TABLE DONATUR (
	nomor_identitas VARCHAR(20) PRIMARY KEY,
	email VARCHAR(50) NOT NULL,
	nama VARCHAR(50) NOT NULL,
	npwp CHAR(20) NOT NULL,
	no_telp VARCHAR(20),
	alamat VARCHAR(50) NOT NULL,
	username VARCHAR(20) NOT NULL,
	FOREIGN KEY (username) REFERENCES PENGGUNA(username) ON UPDATE CASCADE 
	ON DELETE CASCADE
);

CREATE TABLE ADMIN (
	username VARCHAR(20) PRIMARY KEY,
	FOREIGN KEY(username) REFERENCES PENGGUNA(username) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE INDIVIDUAL_DONOR (
	nik CHAR(16) PRIMARY KEY,
	nomor_identitas_donatur VARCHAR(20) NOT NULL,
	FOREIGN KEY(nomor_identitas_donatur) REFERENCES DONATUR(nomor_identitas) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE YAYASAN (
	no_sk_yayasan VARCHAR(20) PRIMARY KEY,
	email VARCHAR(50) NOT NULL,
	nama VARCHAR(50) NOT NULL,
	no_telp_cp VARCHAR(20),
	nomor_identitas_donatur VARCHAR(20) NOT NULL,
	FOREIGN KEY(nomor_identitas_donatur) REFERENCES DONATUR(nomor_identitas) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE MAHASISWA (
	npm VARCHAR(20) PRIMARY KEY,
	email VARCHAR(50) NOT NULL,
	nama VARCHAR(50) NOT NULL,
	no_telp VARCHAR(20),
	alamat_tinggal VARCHAR(50) NOT NULL,
	alamat_domisili VARCHAR(50) NOT NULL,
	nama_bank VARCHAR(50) NOT NULL,
	no_rekening VARCHAR(20) NOT NULL,
	nama_pemilik VARCHAR(20) NOT NULL,
	username VARCHAR(20) NOT NULL,
	FOREIGN KEY(username) REFERENCES PENGGUNA(username) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE RIWAYAT_AKADEMIK (
	no_urut INT NOT NULL,
	npm VARCHAR(20) NOT NULL,
	semester CHAR(1) NOT NULL,
	tahun_ajaran CHAR(9) NOT NULL,
	jumlah_sks INT NOT NULL,
	IPS DOUBLE PRECISION NOT NULL,
	lampiran VARCHAR(50) NOT NULL, 
	PRIMARY KEY(no_urut, npm)
);

CREATE TABLE SKEMA_BEASISWA (
	kode INT PRIMARY KEY,
	nama VARCHAR(50) NOT NULL,
	jenis VARCHAR(20) NOT NULL,
	deskripsi VARCHAR(50) NOT NULL,
	nomor_identitas_donatur VARCHAR(20) NOT NULL,
	FOREIGN KEY(nomor_identitas_donatur) REFERENCES DONATUR(nomor_identitas) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE SYARAT_BEASISWA (
	kode_beasiswa INT NOT NULL,
	syarat VARCHAR(50) NOT NULL,
	PRIMARY KEY(kode_beasiswa, syarat),
	FOREIGN KEY(kode_beasiswa) REFERENCES SKEMA_BEASISWA(kode) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE SKEMA_BEASISWA_AKTIF (
	kode_skema_beasiswa INT NOT NULL,
	no_urut INT NOT NULL,
	tgl_mulai_pendaftaran DATE NOT NULL,
	tgl_tutup_pendaftaran DATE NOT NULL,
	periode_penerimaan VARCHAR(50) NOT NULL,
	status VARCHAR(20) NOT NULL,
	jumlah_pendaftar INT NOT NULL DEFAULT 0,
	total_pembayaran INT NOT NULL DEFAULT 0,
	PRIMARY KEY(kode_skema_beasiswa, no_urut),
	FOREIGN KEY(kode_skema_beasiswa) REFERENCES SKEMA_BEASISWA(kode) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE PENDAFTARAN (
	no_urut INT NOT NULL,
	kode_skema_beasiswa INT NOT NULL,
	npm VARCHAR(20) NOT NULL,
	waktu_daftar TIMESTAMP NOT NULL,
	status_daftar VARCHAR(20) NOT NULL,
	status_terima VARCHAR(20) NOT NULL,
	PRIMARY KEY(no_urut, kode_skema_beasiswa, npm),
	FOREIGN KEY(no_urut, kode_skema_beasiswa) REFERENCES SKEMA_BEASISWA_AKTIF(no_urut, kode_skema_beasiswa) ON UPDATE CASCADE
	ON DELETE CASCADE,
	FOREIGN KEY(npm) REFERENCES MAHASISWA(npm) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE PEMBAYARAN (
	urutan INT NOT NULL,
	kode_skema_beasiswa INT NOT NULL,
	no_urut_skema_beasiswa_aktif INT NOT NULL,
	npm VARCHAR(20) NOT NULL,
	keterangan VARCHAR(50) NOT NULL,
	tgl_bayar DATE NOT NULL,
	nominal INT NOT NULL,
	PRIMARY KEY(urutan, kode_skema_beasiswa,no_urut_skema_beasiswa_aktif),
	FOREIGN KEY(kode_skema_beasiswa, no_urut_skema_beasiswa_aktif) REFERENCES SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa, no_urut)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(npm) REFERENCES MAHASISWA(npm) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE TABLE TEMPAT_WAWANCARA (
	kode INT PRIMARY KEY,
	nama VARCHAR(50) NOT NULL,
	lokasi VARCHAR(50) NOT NULL
);

CREATE TABLE WAWANCARA (
	no_urut_skema_beasiswa_aktif INT NOT NULL,
	kode_skema_beasiswa INT NOT NULL,
	jadwal TIMESTAMP NOT NULL,
	kode_tempat_wawancara INT NOT NULL,
	PRIMARY KEY(no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, jadwal),
	FOREIGN KEY(kode_skema_beasiswa, no_urut_skema_beasiswa_aktif) REFERENCES SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa, no_urut)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(kode_tempat_wawancara) REFERENCES TEMPAT_WAWANCARA(kode)
	ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PENGUMUMAN (
	tanggal DATE NOT NULL,
	no_urut_skema_beasiswa_aktif INT NOT NULL,
	kode_skema_beasiswa INT NOT NULL,
	username VARCHAR(20) NOT NULL,
	judul VARCHAR(20) NOT NULL,
	isi VARCHAR(255) NOT NULL,
	PRIMARY KEY(tanggal, no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, username),
	FOREIGN KEY(kode_skema_beasiswa, no_urut_skema_beasiswa_aktif) REFERENCES SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa, no_urut)
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(username) REFERENCES PENGGUNA(username) ON UPDATE CASCADE
	ON DELETE CASCADE
);

CREATE OR REPLACE FUNCTION perubahan_jumlah_pendaftar()
RETURNS trigger AS 
$$
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE SKEMA_BEASISWA_AKTIF SET jumlah_pendaftar = jumlah_pendaftar + 1
		WHERE kode_skema_beasiswa = NEW.kode_skema_beasiswa AND no_urut = NEW.no_urut;
		RETURN NEW;
	ELSEIF (TG_OP = 'DELETE') THEN
		UPDATE SKEMA_BEASISWA_AKTIF SET jumlah_pendaftar = jumlah_pendaftar - 1
		WHERE kode_skema_beasiswa = OLD.kode_skema_beasiswa AND no_urut = OLD.no_urut;
		RETURN OLD;
	END IF;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER perubahan_jumlah_pendaftar_trigger
AFTER INSERT OR DELETE
ON PENDAFTARAN FOR EACH ROW
EXECUTE PROCEDURE perubahan_jumlah_pendaftar();

CREATE OR REPLACE FUNCTION perubahan_total_pembayaran()
RETURNS trigger AS
$$
BEGIN
	UPDATE SKEMA_BEASISWA_AKTIF SET total_pembayaran = total_pembayaran + NEW.nominal
	WHERE kode_skema_beasiswa = NEW.kode_skema_beasiswa AND no_urut = NEW.no_urut_skema_beasiswa_aktif;
	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER perubahan_total_pembayaran_trigger
AFTER INSERT
ON PEMBAYARAN FOR EACH ROW
EXECUTE PROCEDURE perubahan_total_pembayaran();

INSERT INTO PENGGUNA(username,password,role) VALUES ('memes0','Elz6tDHI','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cohowbane1','mnMjH0PN','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lwishart2','1SgljYd','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jgodsell3','S9emUtRg9','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('djeanneau4','6gWsZ5Yy19','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lbenton5','L14Bxm4zvFt','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('sgrono6','Tce1JOe0Mv','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('qwindley7','I1XZbn6j5SDn','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ghadland8','QtPiIW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lgenese9','xBkwti6LK','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('eolandaa','a74R4APR9tnb','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dmuckersieb','9qUvRQexRWz0','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('nolczakc','13dCQ2sj','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('csemberd','VQ9EayMl','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('npomerye','T5rQAvFz','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jcassiusf','1w7vBRkV','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mmuckloeg','12Jo3ss8Shf','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('wmacpadeneh','QrxM9ZQqc','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cpactati','ZfdZUADP','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dbrandinoj','dWtFtAlMLL','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('afrankomk','wiRnvH','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bbascombl','LFPs4U0R','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ggodingm','8kPjs2HKadG','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('aleggen','IoZaN6Mkqw','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fantonescuo','KesLPXPh9','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hrobusp','GlKg2tDL','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dlanahanq','ANzRc5rK','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lvalleriner','ernvnc','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('egunthorpes','FsDiZbyS0ou','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('eellowayt','wCey7i','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('aaugaru','7zwCptWVTyyH','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rhobbenv','Zup3UQ8tj80','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fmcbeanw','PnQOMIFzX','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('agawlerx','KZ7RhYRum6pW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tmaggiy','j9JHtK1q','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hiozefovichz','NSnWIW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('pphilipot10','TwkGacfk','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('brichold11','MQVMtiWZ3Gy','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bfelderer12','kc8gJTu358','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hlamp13','h89nHc73','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ameiner14','vx9tqjyhD','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bbeardsall15','PQEf8HcDrmI','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gdeane16','yF3LTDkJLcoo','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('awasiel17','Z3PzAGdt7zW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('statchell18','T1b9m9oWKM','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mjancey19','JUojGbcV7yV','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mdimitriev1a','mF8ekmlzAmPw','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jjohansen1b','E2hL27','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('brickaby1c','GP11mt','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('spalatini1d','916MeKMHFl','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('khully1e','LuGWnYOPQnu','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bbartholin1f','zXdRb7SV1I0J','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mbescoby1g','yIWqcJ4Wait','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jgerkens1h','8dgQhcTllYB','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ghempshall1i','noeQGBOu9','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('sespinazo1j','B8MwTH6wlw','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gcanceller1k','ea9sFdlCgM0D','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dmostin1l','ktaXbopMQB','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dtrangmar1m','Tlh0s2AkXj','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cbrecknell1n','9v6finOLQ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mlambie1o','O8JguZ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('zdahlman1p','UGczlJt','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('nlefeaver1q','SqFaVCu','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jroz1r','Tm6KHKxOYG','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tdunckley1s','IF9nMZdIuK','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bdomeney1t','DWQG0u','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('menocksson1u','LGfslIkOeN1a','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mbyass1v','OZQlwizPxMua','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cclemmens1w','ipxDPervTJ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('emartinello1x','urtbpvb2Ii','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('adouris1y','WUfsQUBDq1SN','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('nveazey1z','q1Swmu','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mcardinale20','63rMsj','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bvivien21','U9ZtwBRArvX','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('aacres22','3F552RY','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('svarey23','zhAYGVu','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tlillee24','KcuB5y','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('trannie25','rgkIzcky','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fmclennan26','gP4KS4kGCZ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('wwiburn27','FZrvrbm','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hdalman28','Eowgw2FSP8w1','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('eprime29','zT9Z1YPX2','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ctilne2a','3YStPh','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('iburhouse2b','wz836ht2eerJ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dwenham2c','1lqTsJEm2','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mhabbershon2d','BOT43cnRDM','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fboatman2e','km6N2PyWKW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('pgonoude2f','qNFgqPEQKby','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ebooton2g','9Ma9lgA','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ngraber2h','rdvcoC3Yo','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jcushworth2i','ffZn6NziRE1k','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rculpan2j','uREKh4E79','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mlillico2k','tg4Ww2aWMjYL','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cbathoe2l','9HUu0kBIZnPl','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mbowstead2m','7m4ciDI0gx','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('skeyson2n','O46EoisS','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dvanichev2o','BkMSGM','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ascorrer2p','QKWQLsORou','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tchallin2q','alTgxxjwDpx','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('wkrabbe2r','QKqpXA4iIZ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('borrow2s','e0wtC1SUdLi0','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('krosenblad2t','cCohYcdQF5N','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fgavaran2u','FXOT7y','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('edhenin2v','a4rA82OnzQ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dcoghlan2w','ZF7KgczkEzWx','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('adebrett2x','4GRL1lrDLRBy','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jughelli2y','daSs96xq3P','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tcrummey2z','bDv3vrqUucmX','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('agadman30','e8uliE','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('edougharty31','yPgE78rbi3','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mgiacobbo32','TtHukEBbDWQj','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('nshears33','tb2B26NuU5JB','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ggounet34','NT3hmM','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rbrenston35','IV3FxMnM','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gseston36','2pfGra7','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('penefer37','k7MMtQLmf','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('prasor38','EjhutpcKTNs1','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('khattiff39','aENeLCRp9Bp','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('asiggens3a','gQp0Xrpydmc','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fsnoding3b','rTCgjW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ningall3c','xqYi9C2S','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('elimeburn3d','9CZjqX2Bn','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mriddock3e','iH0RbOQJwKDV','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tnuzzti3f','ecv4pfTzN','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cconnechy3g','k69tsRhR','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jtopley3h','HCbbzcO','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('emcaree3i','Krc8c6ne','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('vsongist3j','yh3QGNsIzq','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('icant3k','TAqsMVYD1qW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bsanchis3l','y3piS53','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hglawsop3m','mP0HAu','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tcuskery3n','MARapAy7D','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('xmacgowing3o','8griNFb','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bfisher3p','u8Hxht','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('pmcdonald3q','pbnL22hzZs31','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('igolt3r','X7ikmlm','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dreimer3s','BIn4ZfX3axc','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('djeavons3t','5q6kuYNfG','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fwoltering3u','8FAEqx','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kboughtwood3v','YhU0c5Dcj4w','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('schippendale3w','bJUUoWfB','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jabeles3x','TMqB02Q','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fridolfi3y','scioUyqcWq','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ryurkin3z','63gB8fC','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('msokell40','azLTTiMra','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('amoncarr41','3fok2MhQtSRg','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fvannozzii42','daa8H4BZ6lVT','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hfowle43','w1pGX4Ov','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mbernet44','t2HgiuxsInZl','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gtulk45','tDZxybI','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jtetther46','UFAdOfH','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rmackinnon47','1dUJT44tuJ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('crenoden48','PaKes7nwQ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('wpatershall49','t3Mg1JGqTHM','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bcutress4a','EsV9JiC36GN','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tnisby4b','BnCgV2VM3','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('eduffy4c','3qiAL4','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ndudden4d','wBP7Bz6aT','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tshearstone4e','Pz326i','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cjansey4f','f0PSLoeHOWr','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('nfolkard4g','2CYLu8nNTa4C','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('smcqueen4h','lneffhxOk','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('brance4i','rDMRvrnp','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mphilipet4j','X53SMMqWC9RS','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kruggles4k','Lf4qzXz','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('swraighte4l','y0kvpTuXEiN','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ahradsky4m','tkeDs6i3t','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jmee4n','tNzT4r9Yv','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hiannuzzelli4o','svkr0ElmS','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kcaulton4p','B9ZqwRdiF','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hgrunwall4q','yUAL3bPVElbP','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bpetruk4r','lFUAHId','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bwyne4s','q8fws19W','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cmacquarrie4t','7jO6e2','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ksegot4u','G3Qo8BR','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cronaghan4v','FKKNwvg','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hwollacott4w','d3z97MQ87H3','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('garnholtz4x','gbOcx4','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rgiovannazzi4y','jJXazJIFBaZ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lcaselli4z','Yfep5wu6wG','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gskechley50','pTPspLeM8dt','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ddurkin51','02Dw3ZL','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kbuckeridge52','41su02LjK','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lcinderey53','ZRohpvv','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dmantripp54','fqwcVTg','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lgisbourn55','u0KkqdSikhr','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('oplumley56','mo8AnJsJmd','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('nmaciunas57','2z7TgnCTeY','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tteape58','ez6iUwaBRi9z','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bwoliter59','b3fKIEW','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fsandiland5a','Zz8IftvI','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('fsanson5b','BoSehS','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cizchaki5c','XnAvAaLp','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('manney5d','BH5oNQX3gf','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('neaseman5e','kOshaWo3b','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('oturfrey5f','iqBoY9S','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('amacfie5g','9KibgVw','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hchaudret5h','odndsfZ','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mpaggitt5i','8ZAo1qB','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mthompstone5j','1rIQZl','mahasiswa');
INSERT INTO PENGGUNA(username,password,role) VALUES ('btremmel0','a3ooNY','admin');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ngabby1','80C0eyvi2tyU','admin');
INSERT INTO PENGGUNA(username,password,role) VALUES ('eshearstone2','PouF0vRBfK','admin');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kpython3','60EcOcOCjz0','admin');
INSERT INTO PENGGUNA(username,password,role) VALUES ('twix4','IqOjROBWC7N','admin');
INSERT INTO PENGGUNA(username,password,role) VALUES ('drosbrough0','TrQBmbmA','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rbispham1','zVIOOlFKq','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kwoodcraft2','Ykxy6qb','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('sleech3','VtjqfKNw','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gcarayol4','4PablgXfz5','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rchamney5','Mdxz5JYramS','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('cclifford6','9gDJCUGCXZZH','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('evignaux7','Mvmbgc','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kbrachell8','bUHaNEcgn','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('wslyman9','ic8paCYrF','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('thowbrooka','7XMptUOt','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('smithonb','hUvX33Gl','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('knaperc','tKPHy5','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gcopod','b8dhGU','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dslye','WSweMFmx5p6','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('llystef','KiZ9aV','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('clavrinovg','yOQ7h5cb','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('pattenbrowh','noKQgZ','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kkytei','x2SIcBmVcXs','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bhenworthj','oIFjyMQn6XY','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mcheatlek','0GjdVf0','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('uallixl','VezZPVb1Tqt','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('kscuffhamm','octD0gE','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('pbardayn','8oI6A8d1','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rclerko','nLBNbFExM1','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('iambrogiop','6tbasXfGZfjo','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('jlanfareq','lsodOCJ','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('gabysr','TxHqiRO','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('orawless','EWgvYNEOE','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ajanecekt','e0XMqDuOYtq','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('lpaueru','g6ustcLRTnVD','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('crawlev','ikmuDU','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('ayousonw','qpdIGV','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('grickellx','qLr7BNH2','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('vfegany','vAVV24qR','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('nlortzingz','YuDv87','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mdrewry10','Xy46BB','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('agullberg11','sKEHq8qeg','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('tbercevelo12','9LVmj8TMoIb9','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hkirvell13','59CU3r','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('mpoznanski14','SMLhA7WF','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('iarrighi15','TDXY7wRncLGk','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('sbambridge16','9sJBi0m','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('keckford17','ke4vWDvH7g','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('epregel18','dYAfsSsaT','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('rjopson19','7eaXyfUpsI8M','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('bcisson1a','20GNnwxqlv','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('hbarley1b','Q1e8ad6QVLc','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('akunert1c','b9oYFCulE','donatur');
INSERT INTO PENGGUNA(username,password,role) VALUES ('dlansly1d','Tv7IR4fVE','donatur');

INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('273-53-3379','lmcglashan0@soup.io','Libby McGlashan','01.115.347.5-117.000','+7-969-572-8095','5 Pawling Plaza','drosbrough0');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('812-36-4503','ggallager1@51.la','Ginnie Gallager','02.455.840.5-117.000','+51-246-606-1523','3868 Golden Leaf Point','rbispham1');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('652-56-5871','wjeste2@npr.org','Wenda Jeste','04.044.684.1-117.000','+7-416-730-4080','76 Maple Wood Drive','kwoodcraft2');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('879-89-2458','scowl3@dmoz.org','Sanford Cowl','06.604.338.1-117.000','+86-940-317-2373','2347 Mosinee Park','sleech3');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('692-30-9579','scrumbie4@privacy.gov.au','Sheri Crumbie','04.044.684.1-117.000','+55-394-687-1278','79 Claremont Avenue','gcarayol4');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('304-21-3154','gphelipeau5@time.com','Gizela Phelipeau','04.098.134.2-117.000','+33-208-199-3677','163 Sauthoff Road','rchamney5');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('141-43-5648','jannice6@dyndns.org','Jeffry Annice','06.296.730.2-117.000','+33-975-341-0172','503 Chive Hill','cclifford6');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('635-97-5749','dbetonia7@youtube.com','Duffie Betonia','01.115.347.5-117.000','+86-516-942-6833','854 Briar Crest Pass','evignaux7');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('349-65-3481','jnezey8@mayoclinic.com','Josi Nezey','01.229.707.3-117.000','+63-814-574-4884','39774 Saint Paul Street','kbrachell8');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('670-60-7705','kjedrachowicz9@walmart.com','Kelly Jedrachowicz','02.455.745.6-117.000','+86-509-136-5614','1507 Darwin Road','wslyman9');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('213-39-2522','rannesliea@princeton.edu','Rhona Anneslie','02.081.705.2-117.000','+48-426-629-7857','4331 Delladonna Road','thowbrooka');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('316-02-4636','lcorringhamb@imdb.com','Leonerd Corringham','02.081.798.7-117.000','+30-990-246-4811','9 Comanche Drive','smithonb');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('116-74-9434','sburridgec@dyndns.org','Sheridan Burridge','01.229.707.3-117.000','+263-121-948-8921','37 Hanson Street','knaperc');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('538-53-5021','zhainsd@scribd.com','Zelig Hains','06.713.928.7-117.001','+86-629-810-1987','6 Vernon Hill','gcopod');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('381-53-3774','tclapstone@taobao.com','Trent Clapston','02.455.745.6-117.000','+86-124-660-6743','696 Bashford Way','dslye');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('447-92-6286','stwidlef@bigcartel.com','Sherwynd Twidle','02.081.798.7-117.000','+374-398-449-1571','6317 Blue Bill Park Road','llystef');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('389-73-6710','aindeg@sciencedirect.com','Angela Inde','04.044.684.1-117.000','+880-856-155-4416','4 Oak Valley Hill','clavrinovg');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('578-39-1132','fwincklesh@utexas.edu','Farris Winckles','04.098.134.2-117.000','+47-480-618-7213','0 Del Sol Point','pattenbrowh');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('581-65-8698','cdundridgei@goo.gl','Cassaundra Dundridge','06.296.730.2-117.000','+86-867-578-0551','159 Village Alley','kkytei');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('798-76-2768','bconnewj@goo.ne.jp','Bili Connew','04.151.624.6-117.000','+48-513-461-5485','9381 Nancy Road','bhenworthj');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('159-18-8109','tokellyk@google.com','Torrie O''Kelly','08.233.253.7-117.001','+1-256-304-1802','82552 Towne Junction','mcheatlek');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('855-99-8786','gkirkmanl@theglobeandmail.com','Gabriel Kirkman','01.229.707.3-117.000','+62-818-522-6801','48365 Hanover Plaza','uallixl');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('334-58-9392','mpersem@acquirethisname.com','Missy Perse','06.604.688.9-117.000','+385-631-562-2220','46620 Forster Park','kscuffhamm');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('664-30-5942','copperyn@cisco.com','Carolann Oppery','04.098.134.2-117.000','+31-952-793-2604','4857 Waubesa Parkway','pbardayn');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('407-62-5854','gakido@reference.com','Gavan Akid','06.604.338.1-117.000','+86-349-459-0287','1 Ryan Way','rclerko');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('839-37-9792','mcasboltp@technorati.com','Mireielle Casbolt','08.233.253.7-117.001','+92-576-831-8280','684 Towne Park','iambrogiop');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('193-24-7367','jtrazziq@house.gov','Jedidiah Trazzi','02.455.840.5-117.000','+1-817-957-7061','4766 Butterfield Road','jlanfareq');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('332-71-0642','tpammenterr@list-manage.com','Tawnya Pammenter','04.098.134.2-117.000','+86-215-608-3278','9 West Alley','gabysr');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('346-33-0854','bhopkynss@ycombinator.com','Bryan Hopkyns','06.604.688.9-117.000','+244-969-747-7016','98 Sloan Court','orawless');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('792-84-2530','vliverockt@google.de','Vinson Liverock','06.604.316.7-117.001','+81-111-155-7715','260 Haas Drive','ajanecekt');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('329-04-4256','ghayballu@utexas.edu','Goran Hayball','06.713.928.7-117.001','+351-211-955-5777','14370 Rigney Trail','lpaueru');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('851-99-3022','afrodshamv@blinklist.com','Alexi Frodsham','08.233.253.7-117.001','+54-417-603-4707','56372 Orin Center','crawlev');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('508-12-5442','llapthornw@icq.com','Lavinie Lapthorn','07.059.380.1-117.000','+86-548-790-5919','69 Brown Park','ayousonw');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('273-47-7527','rpovallx@surveymonkey.com','Ronni Povall','02.081.705.2-117.000','+62-760-533-0649','35491 Rieder Way','grickellx');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('689-55-3316','kunsteady@cornell.edu','Kingsley Unstead','06.052.428.7-117.000','+351-350-346-9119','559 Mockingbird Circle','vfegany');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('120-02-0640','sjenteauz@hostgator.com','Saree Jenteau','01.851.659.1-117.000','+62-737-715-6632','83289 Northwestern Place','nlortzingz');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('362-10-8064','wbroomer10@baidu.com','Winny Broomer','06.604.338.1-117.000','+62-119-451-3551','733 Orin Drive','mdrewry10');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('154-87-6186','mthompson11@seesaa.net','Marylynne Thompson','06.052.428.7-117.000','+7-145-278-0976','99 Hauk Lane','agullberg11');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('705-83-1186','esoanes12@businesswire.com','Eldridge Soanes','02.455.745.6-117.000','+234-595-676-0496','7113 4th Road','tbercevelo12');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('483-72-4696','mbigadike13@bbb.org','Marlene Bigadike','07.059.380.1-117.000','+7-128-175-5375','0 Brown Avenue','hkirvell13');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('480-74-8408','bchristopherson14@ebay.co.uk','Bellina Christopherson','02.455.840.5-117.000','+48-865-139-6316','4052 Red Cloud Avenue','mpoznanski14');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('358-06-7426','sgounet15@linkedin.com','Silvie Gounet','04.044.292.3-117.000','+1-682-788-2709','1 Sherman Drive','iarrighi15');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('651-51-7245','etolmie16@discuz.net','Essie Tolmie','01.115.347.5-117.000','+33-446-219-6943','86271 Menomonie Point','sbambridge16');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('870-53-0483','lcranidge17@list-manage.com','Laird Cranidge','06.296.730.2-117.000','+62-702-611-8904','75 Russell Way','keckford17');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('848-70-1783','minott18@godaddy.com','Melissa Inott','02.081.798.7-117.000','+86-658-999-0940','2 Mallard Alley','epregel18');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('345-42-4656','ocoryndon19@weather.com','Ora Coryndon','02.081.798.7-117.000','+506-212-797-2572','0202 Petterle Circle','rjopson19');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('861-52-2217','cnatte1a@oracle.com','Clementius Natte','02.455.745.6-117.000','+234-519-661-5549','635 Saint Paul Circle','bcisson1a');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('543-99-5297','pbulward1b@newyorker.com','Perceval Bulward','06.604.688.9-117.000','+48-653-262-9702','5336 Forest Place','hbarley1b');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('397-26-3427','tarman1c@mit.edu','Teodoor Arman','04.151.624.6-117.000','+7-202-487-0990','7 Bashford Court','akunert1c');
INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) VALUES ('675-87-5560','mbech1d@eepurl.com','Milka Bech','01.115.347.5-117.000','+48-291-693-7137','92 Monterey Circle','dlansly1d');

INSERT INTO ADMIN(username) VALUES ('btremmel0');
INSERT INTO ADMIN(username) VALUES ('ngabby1');
INSERT INTO ADMIN(username) VALUES ('eshearstone2');
INSERT INTO ADMIN(username) VALUES ('kpython3');
INSERT INTO ADMIN(username) VALUES ('twix4');

INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('698-30-7597','273-53-3379');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('245-41-8643','812-36-4503');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('850-59-7044','652-56-5871');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('457-20-5636','879-89-2458');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('815-37-0038','692-30-9579');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('796-80-1411','304-21-3154');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('327-52-9378','141-43-5648');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('379-71-8253','635-97-5749');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('126-55-0895','349-65-3481');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('163-14-9297','670-60-7705');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('435-45-1763','213-39-2522');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('248-89-5579','316-02-4636');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('491-38-4926','116-74-9434');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('240-34-1382','538-53-5021');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('644-69-9408','381-53-3774');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('473-25-8972','447-92-6286');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('379-47-2645','389-73-6710');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('445-87-8249','578-39-1132');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('252-75-6612','581-65-8698');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('327-05-7386','798-76-2768');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('165-50-5074','159-18-8109');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('204-53-7212','855-99-8786');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('836-37-4932','334-58-9392');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('240-23-2805','664-30-5942');
INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur) VALUES ('272-73-1029','407-62-5854');

INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (2207309169,'kmcgauhy0@usda.gov','Dare, Weissnat and Halvorson','123-394-0427','362-10-8064');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (7786184147,'smcquirk1@ehow.com','Dare Inc','356-771-3482','154-87-6186');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (9062565794,'gcassam2@who.int','Volkman, Harris and Larkin','546-189-5025','792-84-2530');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (6731025038,'kgounod3@networksolutions.com','O''Connell-Okuneva','388-909-3425','861-52-2217');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (9639971758,'cfludgate4@squarespace.com','Cummerata-Mohr','450-324-2612','345-42-4656');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (6940884580,'sransfield5@stanford.edu','Boyle, Stamm and Toy','254-344-4511','861-52-2217');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (0898124506,'ahannen6@nytimes.com','Kuhlman, Pacocha and Hane','816-575-2891','851-99-3022');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (2652254545,'bwardlaw7@constantcontact.com','Runte, Russel and Roob','600-364-9323','675-87-5560');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (8168559606,'fsalterne8@rambler.ru','Bailey, Greenholt and Anderson','583-716-0977','848-70-1783');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (3222318948,'mburroughes9@prnewswire.com','Marquardt-Rippin','970-780-0984','329-04-4256');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (2826859900,'eszimonia@facebook.com','Dietrich Group','513-755-8833','273-47-7527');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (7802314151,'jgrosvenorb@irs.gov','Reichel LLC','501-536-6611','480-74-8408');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (1610111184,'ahawtreyc@delicious.com','Daniel Inc','205-760-2161','870-53-0483');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (6764709955,'cdimmned@shareasale.com','Smith Inc','206-673-0074','861-52-2217');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (0665467311,'wghidinie@google.nl','Ankunding-Marks','475-948-1692','358-06-7426');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (7375059306,'pkliemkef@hc360.com','Marks, Conroy and Sauer','333-526-2584','358-06-7426');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (3218749484,'wdavydochg@digg.com','Schmidt-Wilderman','891-580-6053','848-70-1783');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (8737973088,'rbarnardh@google.nl','Weber, Wilderman and Hansen','503-785-7771','362-10-8064');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (2712894073,'ahydesi@liveinternet.ru','Flatley-Kerluke','711-373-3355','154-87-6186');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (2996077407,'gbwyj@elpais.com','Thiel LLC','888-499-6972','848-70-1783');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (7575266623,'ejohnceyk@163.com','Franecki LLC','886-132-1540','346-33-0854');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (5344036183,'msellekl@tamu.edu','Kovacek, Bahringer and Schultz','346-700-4977','273-47-7527');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (8018047995,'rrogerotm@facebook.com','Frami LLC','932-530-6481','705-83-1186');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (4873026822,'lpoguen@dot.gov','Turner-Russel','224-733-6927','154-87-6186');
INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) VALUES (4126005927,'mclampeo@ftc.gov','Abshire, Runte and Reichert','788-741-4393','120-02-0640');

INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('436-65-9829','ahindenburg0@moonfruit.com','Enrika Boswood','326-992-9843','0 Milwaukee Alley','1 Forest Dale Center','BNI',3560929350,'Austin Hindenburg','memes0');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('459-93-1211','cmeenehan1@webs.com','Garner Chable','504-386-0230','4 Grover Trail','50 Manley Parkway','CIMB',7143887331,'Catlin Meenehan','cohowbane1');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('231-38-8932','jcrossland2@jigsy.com','Jacobo Raddan','217-242-3866','03 Eliot Circle','55 Dixon Court','BNI',2056846144,'Jordana Crossland','lwishart2');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('577-33-2818','yjoe3@microsoft.com','Ashlie Posnett','858-157-5803','29081 Longview Way','201 Brentwood Terrace','BCA',2642728072,'Yorke Joe','jgodsell3');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('607-78-0457','fhallows4@accuweather.com','Nickolas Furse','136-892-3197','17688 Anzinger Terrace','18281 Ridge Oak Plaza','CIMB',9038807430,'Fabien Hallows','djeanneau4');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('501-08-9298','csheldon5@dmoz.org','Chaunce Mandry','305-540-5942','85039 Elgar Court','039 Calypso Lane','CIMB',8601489575,'Caritta Sheldon','lbenton5');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('137-04-1781','smacginley6@aol.com','Lizzy Lampitt','853-800-6543','7422 Bunting Park','19109 Gina Circle','BNI',8036894106,'Samantha MacGinley','sgrono6');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('610-93-1211','awinslet7@eventbrite.com','Sophi Crispe','530-776-8410','8360 Spohn Pass','8444 Mcguire Circle','MANDIRI',5943747435,'Aidan Winslet','qwindley7');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('640-47-6234','cfireman8@ebay.co.uk','Heywood Harrell','995-255-1707','4 Holmberg Junction','9440 Hallows Drive','BNI',2809514615,'Clayborne Fireman','ghadland8');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('218-76-5590','afilipczak9@mysql.com','Jocelyn Noonan','173-163-6332','61 Bashford Junction','78 Del Sol Center','BNI',5442643319,'Abigale Filipczak','lgenese9');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('272-49-4540','ccheccuccia@sina.com.cn','Aylmar Dumingo','421-745-6048','4 Lighthouse Bay Park','60 Novick Pass','CIMB',1380781736,'Cinnamon Checcucci','eolandaa');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('366-30-2917','aalbisserb@indiegogo.com','Daron Benaine','259-715-8758','671 Old Shore Junction','80 Monument Parkway','MANDIRI',0127855769,'Ashla Albisser','dmuckersieb');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('764-52-0172','jmaclurec@answers.com','Augusto Pretious','893-573-0986','17 Carioca Hill','772 Forest Dale Hill','CIMB',8405918302,'Jordon MacLure','nolczakc');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('112-38-7004','fmcgurnd@homestead.com','Rafael Sails','929-810-2313','6 Forster Junction','0135 Gale Center','BRI',2345631449,'Francois McGurn','csemberd');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('800-49-7477','rlongcakee@zimbio.com','Shandra Fagg','459-243-3387','121 Northland Circle','7444 Sutteridge Road','BCA',7785545693,'Richmound Longcake','npomerye');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('540-26-1905','hbechef@ow.ly','Bee Longbothom','702-969-9498','972 School Court','7615 Rusk Street','BNI',1940919266,'Herbie Beche','jcassiusf');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('524-86-2895','bwinterborneg@plala.or.jp','Christi Vedmore','689-244-2507','7912 Mitchell Street','83 Crownhardt Terrace','BCA',2861773103,'Basilius Winterborne','mmuckloeg');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('575-02-1762','bmosleyh@nasa.gov','Hayley Bernaldez','894-799-0356','05 Goodland Pass','40096 Arkansas Crossing','CIMB',4521127800,'Becky Mosley','wmacpadeneh');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('758-88-1894','mswadeni@craigslist.org','Ellery Gilling','694-489-9462','5 Eagle Crest Junction','83772 Rockefeller Hill','BRI',6024055552,'Marisa Swaden','cpactati');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('860-88-1628','bdevonshirej@fastcompany.com','Kathie MacElharge','949-453-8027','90 Cottonwood Way','4236 Sachtjen Point','CIMB',2773839471,'Brad Devonshire','dbrandinoj');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('173-84-3529','aklosk@businessinsider.com','Emelia Saunton','646-669-1128','63 Northfield Point','8098 Sunbrook Drive','CIMB',0550686932,'Annabelle Klos','afrankomk');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('139-59-2179','bseagel@paypal.com','Emilee Newman','697-352-8972','8072 Acker Crossing','401 Clemons Center','CIMB',7153195815,'Benetta Seage','bbascombl');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('487-04-2065','hrexm@themeforest.net','Rickie Rubens','178-569-0341','4118 Barby Circle','9 Morning Point','BNI',7341821604,'Hannah Rex','ggodingm');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('581-50-4216','kpaulsn@wikipedia.org','Gilberto Cartwight','457-152-6469','6 Jay Court','79 Anzinger Parkway','MANDIRI',8857511588,'Katleen Pauls','aleggen');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('750-73-0354','fgaythero@blogs.com','Anita Frotton','690-283-7201','5427 Superior Plaza','3487 Kedzie Crossing','BNI',5069068423,'Fulton Gayther','fantonescuo');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('772-21-4355','mdowellp@buzzfeed.com','Lucienne Carlile','956-582-0029','10461 Burning Wood Terrace','7 Shopko Avenue','MANDIRI',0448790564,'Magdalene Dowell','hrobusp');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('334-92-9801','mkinavanq@dailymotion.com','Barnebas Wile','254-717-9097','31024 Fairview Drive','3861 Anniversary Terrace','BNI',6835085932,'Maddy Kinavan','dlanahanq');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('678-53-8288','cmallatrattr@accuweather.com','Leonie Corry','592-731-5498','9 Stone Corner Road','9 Starling Hill','MANDIRI',9520776893,'Clarine Mallatratt','lvalleriner');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('719-95-9060','aambruss@businessinsider.com','Lazare Bearman','922-296-4290','5 Graceland Drive','9 Meadow Vale Point','MANDIRI',6206108805,'Artemas Ambrus','egunthorpes');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('478-21-9115','mhirchet@friendfeed.com','Ada Giraudou','438-280-3709','74 Maywood Street','5 Gale Point','BRI',4723120440,'Monte Hirche','eellowayt');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('767-13-7088','kpeltzu@gov.uk','Pegeen Rentenbeck','582-264-0654','5 Novick Park','11 Kensington Alley','BRI',6380195912,'Kenny Peltz','aaugaru');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('183-15-0861','tvannacciv@intel.com','Michaelina Cradey','918-601-0422','352 Monterey Avenue','9390 Maple Park','MANDIRI',9195707832,'Tanner Vannacci','rhobbenv');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('799-51-2795','cbrickseyw@w3.org','Fonz Fallon','227-482-5118','95 Morrow Circle','91 Jay Park','BNI',4904011287,'Constance Bricksey','fmcbeanw');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('805-66-5681','ssuartx@list-manage.com','Melanie Pirson','615-803-8589','6953 Claremont Crossing','474 Colorado Circle','BRI',2807162282,'Stace Suart','agawlerx');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('347-13-4337','hpolglazey@census.gov','Meir Edinboro','342-256-8672','86 Hagan Pass','84 Mendota Avenue','BNI',5761793083,'Harvey Polglaze','tmaggiy');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('299-81-0637','zstonehousez@canalblog.com','Doralynne Orridge','585-300-9495','49012 Oak Valley Alley','84 Kingsford Trail','BNI',7543464373,'Zarla Stonehouse','hiozefovichz');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('167-48-4916','msprade10@upenn.edu','Kirsti Andreas','177-570-1101','7 Crownhardt Center','7 Briar Crest Junction','BRI',3609395869,'Muffin Sprade','pphilipot10');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('267-29-6700','jtweddell11@plala.or.jp','Leroi Renshall','831-290-7249','9287 Truax Court','35004 Dorton Pass','BRI',3697789309,'Joyous Tweddell','brichold11');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('250-73-7377','nkeig12@discuz.net','Chandler Torns','873-545-8712','0 Bonner Lane','362 Schlimgen Crossing','BNI',8220512024,'Nerte Keig','bfelderer12');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('643-33-6215','rswaysland13@tinyurl.com','Walther Barrand','825-241-2944','626 Ronald Regan Plaza','0 Talisman Park','CIMB',4636253264,'Rafaelia Swaysland','hlamp13');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('217-34-1149','ckornyakov14@google.com.br','Margret Collumbine','197-285-0108','942 Debra Point','2 Northland Court','BRI',2439419311,'Camel Kornyakov','ameiner14');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('450-77-5202','dibarra15@columbia.edu','Renaldo Bickerstaffe','232-461-9858','44 Nancy Alley','01 Atwood Pass','MANDIRI',4189521760,'Derby Ibarra','bbeardsall15');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('645-89-5615','sbinge16@jalbum.net','Miguelita Norris','951-953-7251','92 Veith Park','347 Daystar Road','CIMB',8144154029,'Stewart Binge','gdeane16');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('112-41-4005','baskwith17@bravesites.com','Jammal Sirkett','270-430-3916','65 Maple Wood Way','9 Amoth Pass','CIMB',8474102650,'Bennett Askwith','awasiel17');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('162-66-4042','gtosney18@jigsy.com','Elmer Olsen','560-412-7452','3553 Kinsman Street','32990 Prentice Pass','CIMB',3984469098,'Gerrilee Tosney','statchell18');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('277-94-0551','gcaslin19@zdnet.com','Grayce Fraine','879-466-7498','2 Vidon Hill','83 Hintze Point','BCA',5870021340,'Geoff Caslin','mjancey19');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('896-04-4806','kgarrals1a@github.io','Melisandra Muccino','579-129-7205','167 Lawn Court','4 Sutteridge Circle','BCA',7953420090,'Kyla Garrals','mdimitriev1a');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('152-62-1120','vswindon1b@netscape.com','Celesta Issacson','437-942-8897','693 Northport Street','933 Elgar Crossing','MANDIRI',8093890319,'Vallie Swindon','jjohansen1b');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('708-53-7033','squeyeiro1c@bravesites.com','Tammie Henmarsh','431-235-7387','080 Cottonwood Avenue','263 Spohn Point','BNI',3276790688,'Stephan Queyeiro','brickaby1c');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('811-38-8670','agrimwade1d@tiny.cc','Mady Simpkins','325-169-1992','2 Rigney Alley','6 Chinook Trail','MANDIRI',4589728648,'Almeta Grimwade','spalatini1d');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('772-56-1682','npietrusiak1e@ifeng.com','Rurik Kendred','725-599-0848','0 Almo Circle','7263 Fair Oaks Center','BCA',7478034721,'Neale Pietrusiak','khully1e');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('270-94-3271','cworcester1f@loc.gov','Frants Othick','366-783-9541','9 Clyde Gallagher Pass','31849 Redwing Street','MANDIRI',7857243698,'Cynde Worcester','bbartholin1f');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('531-31-6841','tbullent1g@freewebs.com','Clair Drake','714-560-3160','0 Swallow Alley','688 Magdeline Place','CIMB',3804381944,'Trixy Bullent','mbescoby1g');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('330-72-1244','shegdonne1h@cpanel.net','Andee Whittier','894-803-5016','57 Merry Terrace','9457 Loeprich Way','BNI',4193520293,'Staford Hegdonne','jgerkens1h');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('739-78-3341','ojacqueme1i@businessinsider.com','Aldwin Sleeny','407-402-2829','664 Mockingbird Drive','3 Shoshone Way','MANDIRI',3021275966,'Odelinda Jacqueme','ghempshall1i');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('872-77-5202','slamyman1j@tumblr.com','Vick Joplin','717-778-0159','0907 Truax Junction','47 Dakota Street','BCA',8613578454,'Sayers Lamyman','sespinazo1j');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('192-93-2015','ecostellow1k@e-recht24.de','Bibi O''Dea','955-506-7693','494 North Parkway','94 Toban Junction','BRI',9280202286,'Edgardo Costellow','gcanceller1k');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('354-75-1644','tfiggins1l@chronoengine.com','David Cullabine','669-538-3031','58 Mosinee Center','3 Scott Pass','BNI',6242076268,'Trish Figgins','dmostin1l');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('360-09-7947','gconnerly1m@census.gov','Mavra Brickham','339-613-8233','874 Helena Pass','613 Gerald Street','MANDIRI',8259376679,'Granger Connerly','dtrangmar1m');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('260-56-7555','abulfoot1n@lulu.com','Gardie Whittock','519-571-4948','1963 Maryland Plaza','2 Green Point','CIMB',9652166588,'Appolonia Bulfoot','cbrecknell1n');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('126-86-8003','jhartridge1o@example.com','Jerri Tampin','970-921-1382','361 Evergreen Road','10 Fieldstone Park','BNI',3899760514,'Jessalyn Hartridge','mlambie1o');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('300-93-0862','fpiken1p@npr.org','Aggi Schnieder','530-921-9331','3178 Oriole Alley','89222 Cascade Drive','BRI',1366736729,'Falito Piken','zdahlman1p');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('604-12-4149','tespadero1q@ft.com','Garold Forgan','550-624-2698','1865 Hoard Park','5 Sycamore Hill','CIMB',7117463619,'Tuckie Espadero','nlefeaver1q');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('403-79-8569','tstopforth1r@over-blog.com','Milka Wilmington','119-207-8902','09776 Daystar Pass','8 Roxbury Place','CIMB',9195907408,'Trude Stopforth','jroz1r');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('305-15-6958','ylyttle1s@pen.io','Bianka Ferrini','482-699-1160','59 Stone Corner Junction','752 Crowley Court','BCA',7188284849,'Yolande Lyttle','tdunckley1s');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('820-51-6377','dpinck1t@networksolutions.com','Berna Spolton','285-262-6916','3646 Mccormick Trail','0 Esch Court','CIMB',6422194978,'Dorie Pinck','bdomeney1t');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('166-49-6849','kroast1u@washington.edu','Devy Matchett','931-839-6635','7 Superior Place','32 Bartillon Circle','BNI',4207572295,'Kayla Roast','menocksson1u');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('154-34-4720','wfransewich1v@constantcontact.com','Annecorinne Beamiss','931-785-8060','8707 Buena Vista Park','22 Mandrake Park','CIMB',1408554798,'Wittie Fransewich','mbyass1v');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('457-97-6704','astoves1w@youtu.be','Mart Quincee','612-878-7022','34 Armistice Crossing','97 Thompson Park','BNI',1609039726,'Allene Stoves','cclemmens1w');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('315-96-8326','bmawer1x@psu.edu','Barnabas Pavlenko','698-359-3560','0 Continental Place','5 Oak Valley Terrace','CIMB',8102438959,'Bertrando Mawer','emartinello1x');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('520-97-1804','tmedwell1y@oakley.com','Tripp Witherby','129-308-8994','9779 Calypso Circle','8 Sutherland Lane','MANDIRI',6708390876,'Thain Medwell','adouris1y');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('513-38-4985','mgiacomuzzi1z@163.com','Janeva Pilling','483-522-4811','96 Magdeline Hill','7422 1st Point','MANDIRI',5282307103,'Malia Giacomuzzi','nveazey1z');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('494-03-2037','pscruton20@columbia.edu','Rowen Siemons','976-216-6783','0306 Bay Center','70471 Schlimgen Center','CIMB',2543590005,'Parnell Scruton','mcardinale20');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('192-74-3706','tbartell21@quantcast.com','Winn Waltho','433-983-3923','88300 Moose Trail','22 Meadow Ridge Hill','BCA',6323283573,'Tatum Bartell','bvivien21');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('407-82-6646','trivelon22@hud.gov','Guendolen Walsham','238-698-9090','30 Sycamore Terrace','12495 Knutson Street','MANDIRI',1175910201,'Tallou Rivelon','aacres22');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('205-96-9725','svaggers23@indiegogo.com','Noni Conkling','230-122-0507','80436 Ridgeway Alley','5869 Farmco Road','BNI',7789983911,'Sibella Vaggers','svarey23');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('101-96-9317','wpisculli24@newyorker.com','Adrian Leacock','200-783-5323','9262 Hayes Court','87 Northland Circle','BRI',0996617361,'Winnah Pisculli','tlillee24');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('529-31-4852','cattrey25@lycos.com','Onida Konerding','292-797-4669','54705 Aberg Park','49633 Lakewood Point','BCA',9630102609,'Cherida Attrey','trannie25');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('149-21-8324','sgives26@privacy.gov.au','Gilberta Blaylock','668-165-0747','5216 Steensland Court','84 3rd Crossing','BRI',3412547506,'Stanwood Gives','fmclennan26');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('171-62-0674','oloades27@dedecms.com','Grace Dorney','766-484-1901','2548 Weeping Birch Avenue','34384 Moland Circle','BCA',1269999443,'Olin Loades','wwiburn27');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('130-08-2176','nprigmore28@topsy.com','Gabby Risso','859-430-1623','79 Mallory Parkway','05 Waubesa Hill','CIMB',1717801420,'Nester Prigmore','hdalman28');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('400-13-5516','akynge29@ox.ac.uk','Henrie Easton','884-218-4067','0 Graedel Trail','10 Mitchell Point','MANDIRI',3902053577,'Anatola Kynge','eprime29');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('604-11-0062','apulham2a@ning.com','Sharyl Dellenbroker','503-427-0723','46400 Towne Lane','62825 Scoville Park','BNI',1564473791,'Anderea Pulham','ctilne2a');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('429-17-3913','gbastick2b@hao123.com','Rowan Hulett','477-712-2112','3 Transport Terrace','2435 Moland Trail','CIMB',0487028570,'Gillian Bastick','iburhouse2b');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('180-27-7430','cherculson2c@japanpost.jp','Dominique Geistbeck','560-949-1776','433 Charing Cross Avenue','32377 Michigan Trail','CIMB',3605421814,'Charline Herculson','dwenham2c');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('825-75-6353','jlillie2d@linkedin.com','Bartlett Larvor','213-697-5586','1 Ryan Trail','7 Golf View Place','MANDIRI',5148650109,'John Lillie','mhabbershon2d');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('754-19-0412','kscobie2e@tamu.edu','Heall Benting','766-410-3815','0 Old Shore Lane','098 Brown Court','MANDIRI',3382630567,'Karla Scobie','fboatman2e');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('381-27-6538','achelsom2f@godaddy.com','Tera Baughan','813-996-9154','535 Union Trail','2895 Mcguire Way','BCA',3839228913,'Albrecht Chelsom','pgonoude2f');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('157-34-2514','dbayliss2g@youtube.com','Fitzgerald Gavaghan','228-128-7717','3696 Ridgeway Pass','8 Melrose Alley','CIMB',1742744214,'Dani Bayliss','ebooton2g');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('323-82-8530','elaidler2h@businessinsider.com','Mason Lashmore','750-179-1538','6 Lake View Road','60 American Crossing','BRI',1880208369,'Ethe Laidler','ngraber2h');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('625-32-9303','mpearmain2i@ovh.net','Olag Ivimey','839-125-1506','119 Anniversary Alley','4 Warbler Center','BRI',2456481682,'Merrili Pearmain','jcushworth2i');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('482-60-9059','mslader2j@psu.edu','Hyatt Magarrell','970-353-9989','577 Raven Plaza','0 Declaration Crossing','CIMB',5501307995,'Moises Slader','rculpan2j');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('195-19-9781','lcranfield2k@gravatar.com','Darrick Gresty','103-684-6873','9 Bluejay Drive','4 Bashford Drive','CIMB',8668876414,'Layton Cranfield','mlillico2k');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('778-65-4685','iparbrook2l@aol.com','Ardyce Margerrison','521-946-3645','818 Ohio Hill','9 Hintze Lane','MANDIRI',2635079382,'Ida Parbrook','cbathoe2l');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('156-18-6654','aemps2m@blogger.com','Ermengarde Flannery','100-928-6773','42 Buena Vista Point','09 American Junction','BCA',8540320169,'Anatole Emps','mbowstead2m');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('130-80-0636','fmasser2n@amazon.com','Marcela Macconaghy','774-735-7199','703 Hermina Lane','03 Arkansas Place','BNI',7760275609,'Florian Masser','skeyson2n');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('708-76-7859','mfomichkin2o@hao123.com','Gennie Akess','413-587-6304','736 Shelley Way','6689 Swallow Street','BCA',2796434192,'Marsha Fomichkin','dvanichev2o');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('743-85-2548','adawkins2p@altervista.org','Lyda Balam','149-986-1593','172 Lyons Hill','7 Graedel Lane','BNI',0572176147,'Ado Dawkins','ascorrer2p');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('114-41-3248','glimming2q@pinterest.com','Elmira Edmondson','476-904-4105','87590 Harper Hill','44 Cottonwood Park','BCA',9002051972,'Gale Limming','tchallin2q');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('840-49-5137','gbrierly2r@bloglines.com','Kassia Blayney','838-927-7577','3 Anthes Junction','7 Haas Avenue','BNI',6591626530,'Gayle Brierly','wkrabbe2r');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('769-75-2987','jcasacchia2s@dot.gov','Odie Keilty','697-818-2518','64693 Melvin Lane','99175 Heath Road','BNI',4661873572,'Janine Casacchia','borrow2s');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('717-67-1233','ssiemandl2t@cbc.ca','Laverna Theseira','538-801-6014','284 American Ash Plaza','020 Village Junction','BRI',9287306044,'Samson Siemandl','krosenblad2t');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('195-77-5893','rde2u@ucoz.ru','Debbie Itzhaki','289-918-2157','7613 Ridgeway Center','2 Bluestem Park','CIMB',1781363315,'Rhetta de Cullip','fgavaran2u');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('683-01-5362','gmattityahou2v@1688.com','Gabey Burnhard','505-421-2272','00065 Bowman Terrace','77 Old Gate Road','BNI',9595617733,'Gussi Mattityahou','edhenin2v');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('539-61-9008','jpeggram2w@plala.or.jp','Donall Cullin','793-457-7606','56345 Morning Circle','97718 Westend Pass','CIMB',4015648420,'Juli Peggram','dcoghlan2w');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('186-44-4787','ccastelletto2x@mtv.com','Bertie Sawley','658-971-9441','2761 Ohio Drive','25493 Toban Plaza','BRI',6331921303,'Conrad Castelletto','adebrett2x');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('135-29-4090','pjatczak2y@etsy.com','Levon Keemer','223-907-6293','438 Sullivan Crossing','0 Northfield Pass','CIMB',8428331278,'Portia Jatczak','jughelli2y');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('346-33-4986','aaslet2z@google.pl','Renard Lethbury','516-353-0890','53 Pierstorff Trail','6331 Clemons Court','MANDIRI',5880564827,'Angelo Aslet','tcrummey2z');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('263-03-0564','melford30@java.com','Adelaide Sambles','223-888-6808','944 Muir Pass','56 Holy Cross Court','BCA',8695818373,'Mirilla Elford','agadman30');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('700-48-0110','mrenzo31@timesonline.co.uk','Kerwin Rotherforth','984-502-3606','2682 Arkansas Lane','84 Vera Park','CIMB',0342422197,'Mariele Renzo','edougharty31');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('494-85-3449','aroderighi32@1688.com','Tabbie Hauxley','959-254-6554','144 Spaight Point','67235 Corry Court','MANDIRI',3139810628,'Avie Roderighi','mgiacobbo32');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('500-61-0847','kgalfour33@slideshare.net','Clarine Farndon','708-201-9072','8 Moland Road','1 Bonner Road','MANDIRI',4853195610,'Kirbie Galfour','nshears33');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('420-42-1229','fsuttling34@abc.net.au','Farlie Crooks','703-977-2642','6 Tomscot Center','9212 3rd Road','BRI',3041316048,'Filberte Suttling','ggounet34');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('573-57-4525','lgauvain35@deliciousdays.com','Georgeta Goodisson','421-911-7173','8 Basil Alley','233 Old Shore Hill','MANDIRI',5647570836,'Lennard Gauvain','rbrenston35');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('878-16-8669','jalsina36@google.ca','Sonya Goldson','268-740-5281','2 Cottonwood Pass','5442 Arkansas Road','BRI',6769319381,'Josephina Alsina','gseston36');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('810-18-3814','lknollesgreen37@state.tx.us','Jenn MacScherie','439-803-0104','53 2nd Parkway','9 Evergreen Crossing','CIMB',3890842887,'Loren Knolles-Green','penefer37');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('263-54-4230','ctort38@mashable.com','Merill Geany','778-658-9213','6886 Jackson Circle','3510 Lakewood Drive','BRI',0946282641,'Craggie Tort','prasor38');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('432-28-1751','amunkley39@cdc.gov','Randene McEnhill','859-410-0745','78436 Arkansas Terrace','48026 Nelson Point','BCA',5667588935,'Arden Munkley','khattiff39');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('287-89-6854','kmeekings3a@ft.com','Felic Sproat','509-843-3532','70237 Oak Valley Plaza','0071 Monument Park','CIMB',1414359748,'Kahaleel Meekings','asiggens3a');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('245-23-3602','sandrioletti3b@reuters.com','Falkner Basden','932-341-4650','134 Cody Junction','44 Myrtle Trail','MANDIRI',3720121429,'Susann Andrioletti','fsnoding3b');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('549-38-9063','nbozworth3c@adobe.com','Elenore Bowdrey','364-314-8757','2 Cody Pass','3 Kim Place','BCA',9230116599,'Nariko Bozworth','ningall3c');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('155-83-0178','hquestier3d@clickbank.net','Rose Dybell','737-823-7474','7990 Pleasure Hill','54335 Anniversary Crossing','BNI',3593579375,'Haskel Questier','elimeburn3d');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('600-06-2084','eklyn3e@networkadvertising.org','Lukas Khrishtafovich','897-710-5512','0173 Hermina Circle','411 Heffernan Point','CIMB',5272550528,'Ethan Klyn','mriddock3e');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('778-06-1568','dmallabon3f@people.com.cn','Bennie Cuthbert','745-273-1221','6906 Artisan Drive','7 Stoughton Crossing','BRI',0395199816,'Debi Mallabon','tnuzzti3f');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('438-57-9714','esykora3g@nba.com','Hetty Lindenblatt','733-820-1458','2580 Johnson Circle','93771 Ramsey Point','BNI',0488437512,'Emilia Sykora','cconnechy3g');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('661-41-1910','dbaskerville3h@engadget.com','Danny Waplinton','422-969-5399','1489 Melby Center','99 Lakewood Gardens Point','BNI',0076365263,'Drusi Baskerville','jtopley3h');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('752-03-5546','jhaywood3i@canalblog.com','Nealon Tillot','253-798-1500','34 Southridge Pass','29 Warner Lane','BRI',6751478216,'Jarib Haywood','emcaree3i');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('615-35-6599','rlitel3j@technorati.com','Annmaria Yakebovich','346-960-4557','385 American Ash Circle','8 Susan Park','CIMB',5692449609,'Roddie Litel','vsongist3j');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('486-01-0939','rprice3k@nyu.edu','Pru MacBean','525-569-5243','9474 Anhalt Lane','2226 Ramsey Trail','CIMB',0253530075,'Rhianna Price','icant3k');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('828-17-7829','hasbery3l@senate.gov','Gerri Drinkwater','367-596-2229','9 Katie Park','8 Thompson Drive','CIMB',6553398941,'Hillery Asbery','bsanchis3l');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('247-76-3081','rduke3m@toplist.cz','Even Beisley','351-473-6681','1863 Sundown Trail','75161 Mesta Alley','MANDIRI',3555915150,'Rosco Duke','hglawsop3m');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('457-12-9301','gchellingworth3n@1688.com','Chantalle O'' Gara','435-352-9428','931 New Castle Drive','74 Columbus Terrace','CIMB',1684835372,'Goldy Chellingworth','tcuskery3n');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('453-42-1885','fstelfox3o@utexas.edu','Gerrie Madigan','611-566-0956','1 Pond Park','64 Victoria Alley','BCA',8105873338,'Federica Stelfox','xmacgowing3o');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('401-78-6216','kbiddiss3p@home.pl','Amy McGillivrie','146-880-2723','8 Westridge Point','681 Little Fleur Crossing','MANDIRI',0045670773,'Kizzie Biddiss','bfisher3p');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('662-64-6798','bladon3q@columbia.edu','Sasha Booler','608-196-3733','25 Arrowood Crossing','51 Cambridge Center','BRI',8685098556,'Basilio Ladon','pmcdonald3q');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('783-84-1288','gnormavill3r@seattletimes.com','Jerrie Twamley','589-847-8164','2818 Sachtjen Court','972 High Crossing Street','MANDIRI',1094107409,'Gwynne Normavill','igolt3r');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('888-21-7510','mmacgaughie3s@yellowbook.com','Devondra McRory','990-757-7963','965 Kingsford Drive','30387 Bluestem Crossing','BCA',0487216431,'Morrie MacGaughie','dreimer3s');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('334-26-0597','gduckerin3t@scientificamerican.com','Nalani Stanaway','750-470-7844','3 Brown Junction','5 Londonderry Drive','BCA',8591278925,'Georgette Duckerin','djeavons3t');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('254-24-5223','cpoles3u@unc.edu','Benedikt Lorait','345-446-2235','82901 Dottie Plaza','08 Emmet Road','MANDIRI',1588016390,'Christin Poles','fwoltering3u');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('562-33-8523','mchristauffour3v@oakley.com','Armando Taree','679-476-0020','0 Grim Center','555 Orin Court','BRI',9035604113,'Marshall Christau','kboughtwood3v');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('183-64-9005','ebucknall3w@hexun.com','Harlie Clapham','365-954-3663','3015 Esker Lane','7813 Rieder Center','MANDIRI',9830756769,'Edith Bucknall','schippendale3w');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('339-37-9748','awestnage3x@mayoclinic.com','Laurence Penniall','938-357-6008','5 Valley Edge Point','8 Briar Crest Avenue','BCA',6317517584,'Abbe Westnage','jabeles3x');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('364-98-5454','aduthie3y@ftc.gov','Hillie Gilham','240-401-6345','4873 Twin Pines Center','773 Browning Point','MANDIRI',3234576009,'Alvinia Duthie','fridolfi3y');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('116-70-4666','mvinck3z@google.com.hk','Steffie Andersson','478-897-2012','6067 Maryland Parkway','1 Amoth Road','CIMB',7103363250,'Marchall Vinck','ryurkin3z');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('737-03-5339','ljervoise40@wordpress.com','Roy Lorkin','439-998-4963','90 Cardinal Court','625 Darwin Place','BNI',3135750000,'Lonnard Jervoise','msokell40');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('385-59-9204','agyurkovics41@yellowbook.com','Pegeen Titcom','558-853-5086','5 Fremont Terrace','58 Lighthouse Bay Avenue','BRI',3801957756,'Aurelia Gyurkovics','amoncarr41');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('761-90-5115','smethven42@amazon.de','Bucky Caughey','361-815-2623','71758 Di Loreto Way','98 Lillian Alley','MANDIRI',4708042507,'Shelley Methven','fvannozzii42');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('316-51-5431','hsonger43@creativecommons.org','Eb Reasce','726-135-5353','72282 Kropf Place','1365 Cambridge Point','BRI',9846125801,'Hunter Songer','hfowle43');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('575-26-9464','ttenby44@ning.com','Hunfredo Neads','476-613-0208','119 Meadow Ridge Way','0 Comanche Pass','BRI',6035887996,'Theadora Tenby','mbernet44');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('136-59-1434','aleyson45@newyorker.com','Thaddus Eatock','442-603-2433','41 Washington Junction','18 Scoville Junction','BCA',0559110332,'Andy Leyson','gtulk45');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('660-53-9592','tmilburne46@cmu.edu','Jephthah Aprahamian','959-261-5968','84696 Drewry Road','9 Schurz Avenue','MANDIRI',6154588100,'Tedd Milburne','jtetther46');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('588-08-1782','cdicks47@moonfruit.com','Gay Jeste','797-667-8684','66 Derek Place','4344 Rockefeller Way','CIMB',6821531293,'Caralie Dicks','rmackinnon47');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('778-72-4710','sbrittian48@yandex.ru','Morie Lighten','352-587-6025','7890 Corben Avenue','560 Westridge Court','BRI',9929766766,'Sioux Brittian','crenoden48');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('589-80-1119','agreenhalf49@rediff.com','Clerc Gilbart','586-598-2272','5564 Summerview Road','4 Columbus Way','BRI',1648639232,'Aveline Greenhalf','wpatershall49');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('526-37-2889','cfahy4a@cdc.gov','Jami Iamittii','533-990-4923','7 Mayer Road','5 Bellgrove Road','BRI',6631762677,'Cozmo Fahy','bcutress4a');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('862-62-4419','jyeskov4b@foxnews.com','Elyn Lindfors','576-417-5733','70223 Twin Pines Point','70220 Trailsway Drive','CIMB',9993148814,'Jacobo Yeskov','tnisby4b');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('671-01-5530','aeykel4c@booking.com','Margo Antonetti','534-616-9767','6 Iowa Circle','97451 Debra Way','MANDIRI',6104882498,'Ailbert Eykel','eduffy4c');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('461-03-8181','ekeenan4d@archive.org','Upton Lusk','154-262-2391','960 Talmadge Court','943 Amoth Pass','CIMB',2874799262,'Elbertina Keenan','ndudden4d');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('449-33-4279','ogrishakin4e@digg.com','Tiphany Bernardt','806-631-7367','2 Larry Terrace','20685 Muir Circle','MANDIRI',1942788975,'Osmond Grishakin','tshearstone4e');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('835-12-1920','jglitherow4f@privacy.gov.au','Keary Ducker','431-167-3041','9 Thompson Drive','7718 Bultman Court','CIMB',4663502768,'Javier Glitherow','cjansey4f');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('574-45-9858','rartindale4g@pinterest.com','Ania Linskill','270-691-0204','6 Vera Alley','7195 Old Shore Terrace','BRI',6066794191,'Rhianna Artindale','nfolkard4g');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('825-82-5294','mvannuchi4h@google.com.br','Launce Colafate','503-632-1354','8 Merchant Junction','5 Fair Oaks Court','MANDIRI',1009242431,'Mandie Vannuchi','smcqueen4h');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('575-31-6793','bwattins4i@yelp.com','Reese Backen','426-145-0914','5539 Starling Lane','866 Russell Way','BNI',4744586813,'Beatrisa Wattins','brance4i');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('842-19-3333','palbutt4j@soundcloud.com','Anselma Kersley','896-730-0704','690 Northwestern Circle','810 Anthes Way','BNI',8205695504,'Percy Albutt','mphilipet4j');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('657-81-2475','bfallon4k@squidoo.com','Celia Alven','549-284-2440','210 Golden Leaf Plaza','426 Rowland Place','MANDIRI',5475545723,'Boycey Fallon','kruggles4k');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('192-74-3338','wholah4l@furl.net','Sanford Lacheze','550-131-1595','2 Pond Street','7868 Manufacturers Avenue','CIMB',9711470411,'Wilmar Holah','swraighte4l');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('248-50-9723','wmyers4m@earthlink.net','Bree Khoter','222-300-7678','05 Forest Run Hill','242 Forest Run Pass','BNI',2875044257,'Wilma Myers','ahradsky4m');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('420-11-1467','jmarton4n@google.pl','Wallis Chark','481-830-2795','52505 Union Street','7405 Blackbird Park','BCA',8309729383,'Jere Marton','jmee4n');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('848-03-2313','matkyns4o@accuweather.com','Hayyim Bolduc','990-683-8700','37675 Schlimgen Junction','647 Heath Park','BCA',4630516360,'Marlin Atkyns','hiannuzzelli4o');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('382-47-1743','mbuesden4p@microsoft.com','Fabiano Shearer','333-190-7753','7820 Hoepker Road','122 Meadow Valley Crossing','MANDIRI',8761989134,'Mel Buesden','kcaulton4p');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('650-93-8877','ajeckells4q@weebly.com','Bea Darcey','161-992-5754','9 Independence Parkway','1 Nancy Junction','MANDIRI',9209405560,'Akim Jeckells','hgrunwall4q');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('698-55-9039','ereboul4r@pbs.org','Muffin Whybrow','389-362-9691','97 Jana Road','0531 Village Green Place','MANDIRI',1759453609,'Eldin Reboul','bpetruk4r');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('308-48-3114','glapre4s@ebay.co.uk','Gregor Gilbank','312-762-0439','5923 Marcy Lane','214 Forster Terrace','CIMB',1602447268,'Gino Lapre','bwyne4s');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('335-36-6097','cstukings4t@weebly.com','Tybie Baile','873-469-6339','6876 Maple Center','8559 Bluejay Court','BNI',2898691607,'Cordie Stukings','cmacquarrie4t');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('287-79-1912','lgreenwell4u@bigcartel.com','Aubrey Dodell','926-675-3653','67 Havey Road','5207 Karstens Alley','BRI',2234578213,'Loy Greenwell','ksegot4u');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('884-52-5050','vgillam4v@slideshare.net','Maribelle Pawlata','255-971-5971','92 Calypso Court','586 Twin Pines Crossing','BRI',6832598181,'Vernon Gillam','cronaghan4v');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('813-09-6519','njunes4w@about.com','Ev Dodshun','940-906-0924','4937 Waubesa Trail','36 Autumn Leaf Street','BNI',1241207593,'Nicolais Junes','hwollacott4w');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('178-25-3315','rcollie4x@accuweather.com','Kean Haycox','940-714-1767','791 Thierer Crossing','3205 Northfield Junction','BNI',6268970667,'Robinet Collie','garnholtz4x');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('626-87-7329','jkirkland4y@dyndns.org','Eddy Millin','721-810-9708','82 Nelson Trail','84 Brown Lane','BNI',6199992903,'Jacquelynn Kirkland','rgiovannazzi4y');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('483-14-0808','sclibbery4z@netvibes.com','Marlena Strathern','995-795-0633','9804 Summit Pass','0393 Holmberg Parkway','BNI',2111504084,'Sophey Clibbery','lcaselli4z');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('600-87-7392','dkinkead50@creativecommons.org','Kalina Corbally','493-651-6810','4063 Farmco Point','2035 Golf Course Street','BCA',9369595953,'Dieter Kinkead','gskechley50');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('551-05-3306','mghelarducci51@google.ru','Felic Fasham','923-904-1698','81273 Drewry Place','341 Steensland Center','CIMB',2655662458,'Marguerite Ghela','ddurkin51');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('674-95-6828','kklamp52@parallels.com','Daisie Stonhouse','375-684-5707','3342 Randy Alley','0089 Glacier Hill Center','BCA',1232693294,'Kelsy Klamp','kbuckeridge52');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('867-51-0570','jbembrick53@issuu.com','Kara-lynn McPartling','441-551-4948','3116 Mcbride Way','65 Golf Course Circle','BNI',5005411844,'Jordain Bembrick','lcinderey53');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('795-49-8593','bfarrin54@tiny.cc','Letitia Fries','596-839-5874','6 Reinke Circle','65303 Kedzie Alley','BCA',6775421184,'Brander Farrin','dmantripp54');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('640-68-7539','gambroisin55@china.com.cn','Nichol Nuzzi','246-950-9614','766 Dexter Pass','6 Briar Crest Lane','BRI',1217474897,'Glyn Ambroisin','lgisbourn55');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('386-20-8653','mbadam56@altervista.org','Bellanca Trewman','289-778-0788','4 Logan Parkway','6710 Dapin Center','MANDIRI',5098674735,'Maynard Badam','oplumley56');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('231-67-9564','lneenan57@hao123.com','Krisha Thackwray','683-363-4040','910 Crownhardt Parkway','9 Nelson Junction','BNI',8113685294,'Lucille Neenan','nmaciunas57');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('339-08-2187','dbelliard58@yellowpages.com','Lily Noke','544-736-0608','37 Hintze Place','53 Raven Way','CIMB',8341714086,'Dante Belliard','tteape58');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('691-46-8441','hcrepel59@auda.org.au','Rube Bottjer','695-787-3039','5 Anderson Road','7471 Fulton Point','BNI',9930392629,'Hanna Crepel','bwoliter59');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('877-70-9085','bfessions5a@sina.com.cn','Carolynn Pettisall','552-909-2661','692 Independence Circle','8 Packers Road','CIMB',3889551068,'Beatrisa Fessions','fsandiland5a');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('302-83-4871','lswanton5b@discovery.com','Mordecai Woller','241-419-5618','80 Del Sol Crossing','64417 Novick Place','MANDIRI',5975445264,'Lyndy Swanton','fsanson5b');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('480-38-9996','kmikalski5c@artisteer.com','Mathilde Prier','254-768-2454','012 Tony Terrace','17360 Old Gate Park','BCA',2569978582,'Kassie Mikalski','cizchaki5c');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('536-97-8455','sbasindale5d@symantec.com','Sapphira Fleay','209-370-7028','31664 Bluejay Alley','940 Jenifer Lane','BNI',7977345994,'Sonja Basindale','manney5d');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('451-63-1429','fnottle5e@intel.com','Audry Allsop','689-261-3423','378 Stone Corner Crossing','6 Dennis Crossing','BRI',1489802118,'Friedrick Nottle','neaseman5e');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('505-67-2339','cbroscombe5f@plala.or.jp','Melvin Boyfield','482-941-1522','4 Badeau Road','9 Bonner Pass','BNI',7577270628,'Clayborne Broscombe','oturfrey5f');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('442-50-0642','jaubry5g@disqus.com','Oralie O''Sheeryne','737-638-9307','99 Hoepker Place','52057 Karstens Plaza','BCA',1905276273,'Jonie Aubry','amacfie5g');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('116-86-8009','rsimmonds5h@telegraph.co.uk','Maribeth Pionter','381-393-6018','6964 Schmedeman Drive','9247 Mallard Circle','BCA',2708203274,'Rhianna Simmonds','hchaudret5h');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('743-12-5145','bspeight5i@godaddy.com','Jae Pifford','586-485-4856','0 Mcguire Plaza','298 Hagan Road','CIMB',4818411450,'Brigit Speight','mpaggitt5i');
INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) VALUES ('797-72-6993','dmorfell5j@bloglines.com','Kissiah Hayles','475-235-7048','30586 Thackeray Lane','2271 Ridgeview Pass','BNI',3066134849,'Debi Morfell','mthompstone5j');

INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'436-65-9829',1,'2016/2017',21,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'436-65-9829',2,'2016/2017',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'436-65-9829',3,'2017/2018',22,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'436-65-9829',4,'2017/2018',23,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'436-65-9829',5,'2018/2019',21,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'436-65-9829',6,'2018/2019',23,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'436-65-9829',7,'2019/2020',21,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'436-65-9829',8,'2019/2020',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'459-93-1211',1,'2016/2017',21,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'459-93-1211',2,'2016/2017',23,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'459-93-1211',3,'2017/2018',21,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'459-93-1211',4,'2017/2018',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'459-93-1211',5,'2018/2019',23,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'459-93-1211',6,'2018/2019',21,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'459-93-1211',7,'2019/2020',21,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'459-93-1211',8,'2019/2020',21,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'231-38-8932',1,'2016/2017',21,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'231-38-8932',2,'2016/2017',24,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'231-38-8932',3,'2017/2018',21,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'231-38-8932',4,'2017/2018',21,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'231-38-8932',5,'2018/2019',23,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'231-38-8932',6,'2018/2019',22,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'231-38-8932',7,'2019/2020',22,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'231-38-8932',8,'2019/2020',22,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'577-33-2818',1,'2016/2017',22,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'577-33-2818',2,'2016/2017',23,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'577-33-2818',3,'2017/2018',21,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'577-33-2818',4,'2017/2018',23,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'577-33-2818',5,'2018/2019',24,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'577-33-2818',6,'2018/2019',24,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'577-33-2818',7,'2019/2020',21,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'577-33-2818',8,'2019/2020',22,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'607-78-0457',1,'2016/2017',22,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'607-78-0457',2,'2016/2017',22,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'607-78-0457',3,'2017/2018',22,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'607-78-0457',4,'2017/2018',23,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'607-78-0457',5,'2018/2019',22,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'607-78-0457',6,'2018/2019',23,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'607-78-0457',7,'2019/2020',22,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'607-78-0457',8,'2019/2020',21,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'501-08-9298',1,'2016/2017',21,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'501-08-9298',2,'2016/2017',24,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'501-08-9298',3,'2017/2018',21,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'501-08-9298',4,'2017/2018',22,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'501-08-9298',5,'2018/2019',24,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'501-08-9298',6,'2018/2019',24,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'501-08-9298',7,'2019/2020',21,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'501-08-9298',8,'2019/2020',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'137-04-1781',1,'2016/2017',23,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'137-04-1781',2,'2016/2017',23,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'137-04-1781',3,'2017/2018',23,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'137-04-1781',4,'2017/2018',23,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'137-04-1781',5,'2018/2019',21,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'137-04-1781',6,'2018/2019',21,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'137-04-1781',7,'2019/2020',21,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'137-04-1781',8,'2019/2020',24,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'610-93-1211',1,'2016/2017',22,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'610-93-1211',2,'2016/2017',23,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'610-93-1211',3,'2017/2018',21,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'610-93-1211',4,'2017/2018',22,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'610-93-1211',5,'2018/2019',21,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'610-93-1211',6,'2018/2019',24,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'610-93-1211',7,'2019/2020',22,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'610-93-1211',8,'2019/2020',23,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'640-47-6234',1,'2016/2017',23,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'640-47-6234',2,'2016/2017',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'640-47-6234',3,'2017/2018',24,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'640-47-6234',4,'2017/2018',24,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'640-47-6234',5,'2018/2019',24,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'640-47-6234',6,'2018/2019',21,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'640-47-6234',7,'2019/2020',23,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'640-47-6234',8,'2019/2020',24,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'218-76-5590',1,'2016/2017',22,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'218-76-5590',2,'2016/2017',22,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'218-76-5590',3,'2017/2018',21,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'218-76-5590',4,'2017/2018',22,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'218-76-5590',5,'2018/2019',24,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'218-76-5590',6,'2018/2019',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'218-76-5590',7,'2019/2020',22,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'218-76-5590',8,'2019/2020',23,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'272-49-4540',1,'2016/2017',23,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'272-49-4540',2,'2016/2017',22,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'272-49-4540',3,'2017/2018',23,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'272-49-4540',4,'2017/2018',22,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'272-49-4540',5,'2018/2019',24,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'272-49-4540',6,'2018/2019',21,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'272-49-4540',7,'2019/2020',21,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'272-49-4540',8,'2019/2020',23,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'366-30-2917',1,'2016/2017',24,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'366-30-2917',2,'2016/2017',23,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'366-30-2917',3,'2017/2018',21,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'366-30-2917',4,'2017/2018',22,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'366-30-2917',5,'2018/2019',23,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'366-30-2917',6,'2018/2019',24,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'366-30-2917',7,'2019/2020',22,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'366-30-2917',8,'2019/2020',24,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'764-52-0172',1,'2016/2017',24,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'764-52-0172',2,'2016/2017',22,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'764-52-0172',3,'2017/2018',23,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'764-52-0172',4,'2017/2018',24,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'764-52-0172',5,'2018/2019',23,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'764-52-0172',6,'2018/2019',22,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'764-52-0172',7,'2019/2020',22,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'764-52-0172',8,'2019/2020',24,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'112-38-7004',1,'2016/2017',24,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'112-38-7004',2,'2016/2017',24,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'112-38-7004',3,'2017/2018',21,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'112-38-7004',4,'2017/2018',22,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'112-38-7004',5,'2018/2019',22,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'112-38-7004',6,'2018/2019',22,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'112-38-7004',7,'2019/2020',21,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'112-38-7004',8,'2019/2020',21,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'800-49-7477',1,'2016/2017',21,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'800-49-7477',2,'2016/2017',24,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'800-49-7477',3,'2017/2018',22,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'800-49-7477',4,'2017/2018',21,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'800-49-7477',5,'2018/2019',21,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'800-49-7477',6,'2018/2019',24,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'800-49-7477',7,'2019/2020',23,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'800-49-7477',8,'2019/2020',22,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'540-26-1905',1,'2016/2017',21,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'540-26-1905',2,'2016/2017',24,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'540-26-1905',3,'2017/2018',23,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'540-26-1905',4,'2017/2018',24,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'540-26-1905',5,'2018/2019',24,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'540-26-1905',6,'2018/2019',21,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'540-26-1905',7,'2019/2020',23,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'540-26-1905',8,'2019/2020',24,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'524-86-2895',1,'2016/2017',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'524-86-2895',2,'2016/2017',21,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'524-86-2895',3,'2017/2018',22,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'524-86-2895',4,'2017/2018',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'524-86-2895',5,'2018/2019',24,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'524-86-2895',6,'2018/2019',23,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'524-86-2895',7,'2019/2020',21,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'524-86-2895',8,'2019/2020',24,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'575-02-1762',1,'2016/2017',22,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'575-02-1762',2,'2016/2017',22,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'575-02-1762',3,'2017/2018',22,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'575-02-1762',4,'2017/2018',24,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'575-02-1762',5,'2018/2019',22,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'575-02-1762',6,'2018/2019',21,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'575-02-1762',7,'2019/2020',23,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'575-02-1762',8,'2019/2020',21,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'758-88-1894',1,'2016/2017',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'758-88-1894',2,'2016/2017',23,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'758-88-1894',3,'2017/2018',24,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'758-88-1894',4,'2017/2018',24,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'758-88-1894',5,'2018/2019',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'758-88-1894',6,'2018/2019',22,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'758-88-1894',7,'2019/2020',21,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'758-88-1894',8,'2019/2020',24,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'860-88-1628',1,'2016/2017',21,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'860-88-1628',2,'2016/2017',21,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'860-88-1628',3,'2017/2018',22,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'860-88-1628',4,'2017/2018',22,3.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'860-88-1628',5,'2018/2019',23,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'860-88-1628',6,'2018/2019',21,3.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'860-88-1628',7,'2019/2020',22,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'860-88-1628',8,'2019/2020',22,3.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'173-84-3529',1,'2016/2017',22,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'173-84-3529',2,'2016/2017',21,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'173-84-3529',3,'2017/2018',23,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'173-84-3529',4,'2017/2018',24,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'173-84-3529',5,'2018/2019',23,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'173-84-3529',6,'2018/2019',21,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'173-84-3529',7,'2019/2020',24,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'173-84-3529',8,'2019/2020',22,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'139-59-2179',1,'2016/2017',21,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'139-59-2179',2,'2016/2017',24,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'139-59-2179',3,'2017/2018',23,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'139-59-2179',4,'2017/2018',21,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'139-59-2179',5,'2018/2019',23,3.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'139-59-2179',6,'2018/2019',22,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'139-59-2179',7,'2019/2020',21,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'139-59-2179',8,'2019/2020',23,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'487-04-2065',1,'2016/2017',21,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'487-04-2065',2,'2016/2017',24,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'487-04-2065',3,'2017/2018',22,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'487-04-2065',4,'2017/2018',24,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'487-04-2065',5,'2018/2019',23,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'487-04-2065',6,'2018/2019',21,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'487-04-2065',7,'2019/2020',21,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'487-04-2065',8,'2019/2020',21,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'581-50-4216',1,'2016/2017',24,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'581-50-4216',2,'2016/2017',23,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'581-50-4216',3,'2017/2018',23,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'581-50-4216',4,'2017/2018',21,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'581-50-4216',5,'2018/2019',21,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'581-50-4216',6,'2018/2019',23,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'581-50-4216',7,'2019/2020',24,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'581-50-4216',8,'2019/2020',23,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'750-73-0354',1,'2016/2017',22,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'750-73-0354',2,'2016/2017',24,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'750-73-0354',3,'2017/2018',24,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'750-73-0354',4,'2017/2018',24,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'750-73-0354',5,'2018/2019',21,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'750-73-0354',6,'2018/2019',23,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'750-73-0354',7,'2019/2020',21,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'750-73-0354',8,'2019/2020',24,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'772-21-4355',1,'2016/2017',23,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'772-21-4355',2,'2016/2017',22,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'772-21-4355',3,'2017/2018',21,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'772-21-4355',4,'2017/2018',21,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'772-21-4355',5,'2018/2019',21,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'772-21-4355',6,'2018/2019',23,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'772-21-4355',7,'2019/2020',21,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'772-21-4355',8,'2019/2020',23,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'334-92-9801',1,'2016/2017',22,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'334-92-9801',2,'2016/2017',24,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'334-92-9801',3,'2017/2018',22,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'334-92-9801',4,'2017/2018',21,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'334-92-9801',5,'2018/2019',23,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'334-92-9801',6,'2018/2019',24,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'334-92-9801',7,'2019/2020',24,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'334-92-9801',8,'2019/2020',22,4.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'678-53-8288',1,'2016/2017',22,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'678-53-8288',2,'2016/2017',24,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'678-53-8288',3,'2017/2018',24,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'678-53-8288',4,'2017/2018',21,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'678-53-8288',5,'2018/2019',23,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'678-53-8288',6,'2018/2019',23,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'678-53-8288',7,'2019/2020',24,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'678-53-8288',8,'2019/2020',22,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'719-95-9060',1,'2016/2017',24,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'719-95-9060',2,'2016/2017',21,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'719-95-9060',3,'2017/2018',22,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'719-95-9060',4,'2017/2018',22,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'719-95-9060',5,'2018/2019',23,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'719-95-9060',6,'2018/2019',22,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'719-95-9060',7,'2019/2020',21,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'719-95-9060',8,'2019/2020',23,3.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'478-21-9115',1,'2016/2017',22,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'478-21-9115',2,'2016/2017',23,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'478-21-9115',3,'2017/2018',21,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'478-21-9115',4,'2017/2018',22,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'478-21-9115',5,'2018/2019',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'478-21-9115',6,'2018/2019',24,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'478-21-9115',7,'2019/2020',22,3.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'478-21-9115',8,'2019/2020',22,3.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'767-13-7088',1,'2016/2017',23,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'767-13-7088',2,'2016/2017',22,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'767-13-7088',3,'2017/2018',23,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'767-13-7088',4,'2017/2018',23,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'767-13-7088',5,'2018/2019',22,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'767-13-7088',6,'2018/2019',24,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'767-13-7088',7,'2019/2020',21,3.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'767-13-7088',8,'2019/2020',24,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'183-15-0861',1,'2016/2017',21,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'183-15-0861',2,'2016/2017',22,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'183-15-0861',3,'2017/2018',22,4.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'183-15-0861',4,'2017/2018',23,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'183-15-0861',5,'2018/2019',24,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'183-15-0861',6,'2018/2019',22,3.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'183-15-0861',7,'2019/2020',22,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'183-15-0861',8,'2019/2020',24,3.00,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'799-51-2795',1,'2016/2017',24,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'799-51-2795',2,'2016/2017',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'799-51-2795',3,'2017/2018',21,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'799-51-2795',4,'2017/2018',21,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'799-51-2795',5,'2018/2019',24,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'799-51-2795',6,'2018/2019',23,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'799-51-2795',7,'2019/2020',24,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'799-51-2795',8,'2019/2020',21,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'805-66-5681',1,'2016/2017',23,4.00,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'805-66-5681',2,'2016/2017',21,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'805-66-5681',3,'2017/2018',23,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'805-66-5681',4,'2017/2018',22,4.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'805-66-5681',5,'2018/2019',21,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'805-66-5681',6,'2018/2019',23,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'805-66-5681',7,'2019/2020',21,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'805-66-5681',8,'2019/2020',24,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'347-13-4337',1,'2016/2017',23,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'347-13-4337',2,'2016/2017',24,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'347-13-4337',3,'2017/2018',24,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'347-13-4337',4,'2017/2018',21,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'347-13-4337',5,'2018/2019',21,2.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'347-13-4337',6,'2018/2019',23,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'347-13-4337',7,'2019/2020',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'347-13-4337',8,'2019/2020',22,3.70,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'299-81-0637',1,'2016/2017',23,3.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'299-81-0637',2,'2016/2017',21,3.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'299-81-0637',3,'2017/2018',21,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'299-81-0637',4,'2017/2018',23,2.70,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'299-81-0637',5,'2018/2019',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'299-81-0637',6,'2018/2019',21,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'299-81-0637',7,'2019/2020',24,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'299-81-0637',8,'2019/2020',21,2.33,'Ini lampiran versi 4');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'167-48-4916',1,'2016/2017',23,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'167-48-4916',2,'2016/2017',23,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'167-48-4916',3,'2017/2018',21,3.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'167-48-4916',4,'2017/2018',22,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (5,'167-48-4916',5,'2018/2019',21,2.70,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (6,'167-48-4916',6,'2018/2019',23,2.33,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (7,'167-48-4916',7,'2019/2020',22,2.33,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (8,'167-48-4916',8,'2019/2020',23,3.00,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (1,'267-29-6700',1,'2016/2017',23,2.33,'Ini lampiran versi 1');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (2,'267-29-6700',2,'2016/2017',22,3.00,'Ini lampiran versi 3');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (3,'267-29-6700',3,'2017/2018',24,2.70,'Ini lampiran versi 2');
INSERT INTO RIWAYAT_AKADEMIK(no_urut,npm,semester,tahun_ajaran,jumlah_sks,IPS,lampiran) VALUES (4,'267-29-6700',4,'2017/2018',21,3.33,'Ini lampiran versi 2');

INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (21455,'beasiswa Libby McGlashan','beasiswa olahraga','ini merupakan beasiswa olahraga','273-53-3379');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (89379,'beasiswa Ginnie Gallager','beasiswa penelitian','ini merupakan beasiswa penelitian','812-36-4503');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (61014,'beasiswa Wenda Jeste','beasiswa penelitian','ini merupakan beasiswa penelitian','652-56-5871');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (19348,'beasiswa Sanford Cowl','beasiswa pendidikan','ini merupakan beasiswa pendidikan','879-89-2458');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (71267,'beasiswa Sheri Crumbie','beasiswa olahraga','ini merupakan beasiswa olahraga','692-30-9579');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (11930,'beasiswa Gizela Phelipeau','beasiswa penelitian','ini merupakan beasiswa penelitian','304-21-3154');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (10264,'beasiswa Jeffry Annice','beasiswa olahraga','ini merupakan beasiswa olahraga','141-43-5648');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (82792,'beasiswa Duffie Betonia','beasiswa olahraga','ini merupakan beasiswa olahraga','635-97-5749');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (46796,'beasiswa Josi Nezey','beasiswa penelitian','ini merupakan beasiswa penelitian','349-65-3481');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (92352,'beasiswa Kelly Jedrachowicz','beasiswa olahraga','ini merupakan beasiswa olahraga','670-60-7705');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (45027,'beasiswa Rhona Anneslie','beasiswa penelitian','ini merupakan beasiswa penelitian','213-39-2522');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (26732,'beasiswa Leonerd Corringham','beasiswa penelitian','ini merupakan beasiswa penelitian','316-02-4636');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (65038,'beasiswa Sheridan Burridge','beasiswa pendidikan','ini merupakan beasiswa pendidikan','116-74-9434');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (74390,'beasiswa Zelig Hains','beasiswa olahraga','ini merupakan beasiswa olahraga','538-53-5021');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (29252,'beasiswa Trent Clapston','beasiswa penelitian','ini merupakan beasiswa penelitian','381-53-3774');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (48581,'beasiswa Sherwynd Twidle','beasiswa penelitian','ini merupakan beasiswa penelitian','447-92-6286');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (40120,'beasiswa Angela Inde','beasiswa penelitian','ini merupakan beasiswa penelitian','389-73-6710');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (58544,'beasiswa Farris Winckles','beasiswa pendidikan','ini merupakan beasiswa pendidikan','578-39-1132');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (13623,'beasiswa Cassaundra Dundridge','beasiswa olahraga','ini merupakan beasiswa olahraga','581-65-8698');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (62562,'beasiswa Bili Connew','beasiswa penelitian','ini merupakan beasiswa penelitian','798-76-2768');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (75345,'beasiswa Torrie O''Kelly','beasiswa pendidikan','ini merupakan beasiswa pendidikan','159-18-8109');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (63151,'beasiswa Gabriel Kirkman','beasiswa olahraga','ini merupakan beasiswa olahraga','855-99-8786');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (71816,'beasiswa Missy Perse','beasiswa penelitian','ini merupakan beasiswa penelitian','334-58-9392');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (65306,'beasiswa Carolann Oppery','beasiswa pendidikan','ini merupakan beasiswa pendidikan','664-30-5942');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (94632,'beasiswa Gavan Akid','beasiswa pendidikan','ini merupakan beasiswa pendidikan','407-62-5854');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (61125,'beasiswa Mireielle Casbolt','beasiswa olahraga','ini merupakan beasiswa olahraga','839-37-9792');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (83025,'beasiswa Jedidiah Trazzi','beasiswa olahraga','ini merupakan beasiswa olahraga','193-24-7367');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (96731,'beasiswa Tawnya Pammenter','beasiswa penelitian','ini merupakan beasiswa penelitian','332-71-0642');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (17216,'beasiswa Bryan Hopkyns','beasiswa penelitian','ini merupakan beasiswa penelitian','346-33-0854');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (73760,'beasiswa Vinson Liverock','beasiswa pendidikan','ini merupakan beasiswa pendidikan','792-84-2530');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (95881,'beasiswa Goran Hayball','beasiswa penelitian','ini merupakan beasiswa penelitian','329-04-4256');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (58496,'beasiswa Alexi Frodsham','beasiswa pendidikan','ini merupakan beasiswa pendidikan','851-99-3022');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (43913,'beasiswa Lavinie Lapthorn','beasiswa penelitian','ini merupakan beasiswa penelitian','508-12-5442');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (58279,'beasiswa Ronni Povall','beasiswa penelitian','ini merupakan beasiswa penelitian','273-47-7527');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (68838,'beasiswa Kingsley Unstead','beasiswa olahraga','ini merupakan beasiswa olahraga','689-55-3316');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (83983,'beasiswa Saree Jenteau','beasiswa penelitian','ini merupakan beasiswa penelitian','120-02-0640');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (70890,'beasiswa Winny Broomer','beasiswa penelitian','ini merupakan beasiswa penelitian','362-10-8064');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (63971,'beasiswa Marylynne Thompson','beasiswa olahraga','ini merupakan beasiswa olahraga','154-87-6186');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (89026,'beasiswa Eldridge Soanes','beasiswa pendidikan','ini merupakan beasiswa pendidikan','705-83-1186');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (72694,'beasiswa Marlene Bigadike','beasiswa penelitian','ini merupakan beasiswa penelitian','483-72-4696');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (45205,'beasiswa Bellina Christopherson','beasiswa penelitian','ini merupakan beasiswa penelitian','480-74-8408');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (79608,'beasiswa Silvie Gounet','beasiswa penelitian','ini merupakan beasiswa penelitian','358-06-7426');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (33951,'beasiswa Essie Tolmie','beasiswa olahraga','ini merupakan beasiswa olahraga','651-51-7245');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (84399,'beasiswa Laird Cranidge','beasiswa penelitian','ini merupakan beasiswa penelitian','870-53-0483');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (36096,'beasiswa Melissa Inott','beasiswa olahraga','ini merupakan beasiswa olahraga','848-70-1783');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (93763,'beasiswa Ora Coryndon','beasiswa pendidikan','ini merupakan beasiswa pendidikan','345-42-4656');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (67115,'beasiswa Clementius Natte','beasiswa pendidikan','ini merupakan beasiswa pendidikan','861-52-2217');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (40695,'beasiswa Perceval Bulward','beasiswa pendidikan','ini merupakan beasiswa pendidikan','543-99-5297');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (36488,'beasiswa Teodoor Arman','beasiswa olahraga','ini merupakan beasiswa olahraga','397-26-3427');
INSERT INTO SKEMA_BEASISWA(kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (89029,'beasiswa Milka Bech','beasiswa pendidikan','ini merupakan beasiswa pendidikan','675-87-5560');

INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (21455,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89379,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (61014,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (19348,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (71267,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (11930,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (10264,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (82792,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (46796,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (92352,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (45027,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (26732,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (65038,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (74390,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (29252,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (48581,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (40120,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58544,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (13623,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (62562,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (75345,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (63151,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (71816,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (65306,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (94632,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (61125,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (83025,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (96731,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (17216,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (73760,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (95881,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58496,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (43913,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58279,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (68838,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (83983,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (70890,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (63971,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89026,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (72694,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (45205,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (79608,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (33951,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (84399,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (36096,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (93763,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (67115,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (40695,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (36488,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89029,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (21455,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89379,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (61014,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (19348,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (71267,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (11930,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (10264,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (82792,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (46796,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (92352,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (45027,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (26732,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (65038,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (74390,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (29252,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (48581,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (40120,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58544,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (13623,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (62562,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (75345,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (63151,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (71816,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (65306,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (94632,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (61125,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (83025,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (96731,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (17216,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (73760,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (95881,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58496,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (43913,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58279,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (68838,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (83983,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (70890,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (63971,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89026,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (72694,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (45205,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (79608,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (33951,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (84399,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (36096,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (93763,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (67115,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (40695,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (36488,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89029,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (21455,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89379,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (61014,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (19348,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (71267,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (11930,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (10264,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (82792,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (46796,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (92352,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (45027,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (26732,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (65038,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (74390,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (29252,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (48581,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (40120,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58544,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (13623,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (62562,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (75345,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (63151,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (71816,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (65306,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (94632,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (61125,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (83025,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (96731,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (17216,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (73760,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (95881,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58496,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (43913,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (58279,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (68838,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (83983,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (70890,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (63971,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89026,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (72694,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (45205,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (79608,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (33951,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (84399,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (36096,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (93763,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (67115,'SYARAT3');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (40695,'SYARAT1');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (36488,'SYARAT2');
INSERT INTO SYARAT_BEASISWA(kode_beasiswa,syarat) VALUES (89029,'SYARAT3');

INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (21455,1,'2017/05/22','2018/11/13','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (21455,2,'2017/08/17','2019/01/10','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (89379,1,'2018/01/04','2018/11/28','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (89379,2,'2018/01/11','2018/08/23','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (61014,1,'2017/07/28','2019/01/03','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (61014,2,'2018/04/07','2018/06/22','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (19348,1,'2017/09/23','2018/05/13','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (19348,2,'2017/05/28','2018/08/09','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (71267,1,'2018/02/11','2018/06/11','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (71267,2,'2017/07/23','2018/09/17','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (11930,1,'2017/07/25','2018/06/02','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (11930,2,'2017/08/17','2018/07/26','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (10264,1,'2017/08/12','2019/03/21','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (10264,2,'2017/05/06','2019/02/01','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (82792,1,'2018/03/26','2018/12/07','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (82792,2,'2018/04/25','2019/03/25','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (46796,1,'2017/08/12','2018/10/30','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (46796,2,'2017/12/08','2018/10/20','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (92352,1,'2018/01/23','2019/03/01','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (92352,2,'2018/03/22','2018/11/06','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (45027,1,'2018/04/05','2019/02/09','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (45027,2,'2017/09/21','2019/02/15','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (26732,1,'2017/09/16','2019/01/27','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (26732,2,'2018/03/17','2018/05/22','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (65038,1,'2017/11/19','2019/02/15','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (65038,2,'2017/10/07','2019/01/03','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (74390,1,'2018/04/01','2018/10/17','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (74390,2,'2018/02/20','2018/09/17','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (29252,1,'2018/03/30','2018/12/30','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (29252,2,'2017/11/16','2018/12/23','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (48581,1,'2017/09/22','2018/08/12','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (48581,2,'2017/12/10','2018/12/11','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (40120,1,'2017/06/28','2018/07/17','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (40120,2,'2017/08/12','2018/07/23','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (58544,1,'2017/12/20','2019/04/14','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (58544,2,'2017/07/19','2018/08/14','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (13623,1,'2017/08/01','2018/09/25','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (13623,2,'2017/06/22','2018/12/14','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (62562,1,'2017/12/31','2018/05/25','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (62562,2,'2017/05/02','2018/06/27','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (75345,1,'2017/11/07','2018/05/12','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (75345,2,'2017/08/08','2018/05/02','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (63151,1,'2017/08/01','2018/05/19','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (63151,2,'2017/09/13','2018/05/11','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (71816,1,'2017/11/04','2019/03/05','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (71816,2,'2018/02/06','2018/05/17','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (65306,1,'2018/02/27','2018/08/25','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (65306,2,'2018/04/24','2019/02/14','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (94632,1,'2018/02/12','2018/11/25','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (94632,2,'2017/05/12','2018/09/03','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (61125,1,'2018/02/12','2018/06/24','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (61125,2,'2017/09/10','2019/02/26','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (83025,1,'2017/10/24','2019/02/03','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (83025,2,'2018/02/24','2018/05/01','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (96731,1,'2018/01/12','2018/06/09','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (96731,2,'2017/11/09','2018/08/15','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (17216,1,'2018/01/24','2018/12/14','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (17216,2,'2018/04/18','2018/08/06','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (73760,1,'2017/09/09','2019/04/11','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (73760,2,'2018/02/10','2019/01/13','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (95881,1,'2018/04/16','2019/03/15','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (95881,2,'2018/02/07','2018/11/03','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (58496,1,'2017/06/13','2018/09/03','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (58496,2,'2017/08/20','2018/06/26','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (43913,1,'2017/08/21','2019/01/03','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (43913,2,'2018/02/15','2018/12/12','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (58279,1,'2017/05/29','2018/10/25','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (58279,2,'2017/12/24','2018/10/18','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (68838,1,'2018/03/21','2018/11/01','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (68838,2,'2017/11/08','2018/09/28','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (83983,1,'2018/01/28','2018/09/26','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (83983,2,'2018/02/13','2019/02/21','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (70890,1,'2017/07/21','2019/03/27','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (70890,2,'2018/03/10','2018/07/26','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (63971,1,'2017/11/20','2019/01/10','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (63971,2,'2017/05/28','2019/03/01','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (89026,1,'2017/07/02','2018/08/02','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (89026,2,'2017/08/08','2018/10/04','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (72694,1,'2017/05/07','2019/03/18','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (72694,2,'2017/06/18','2018/08/25','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (45205,1,'2017/06/06','2019/01/24','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (45205,2,'2018/03/20','2019/03/22','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (79608,1,'2017/09/11','2019/04/21','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (79608,2,'2018/01/13','2018/10/26','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (33951,1,'2017/05/12','2018/10/15','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (33951,2,'2017/05/11','2018/11/20','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (84399,1,'2017/10/24','2018/08/26','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (84399,2,'2018/04/11','2018/06/22','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (36096,1,'2017/11/25','2018/08/01','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (36096,2,'2018/02/22','2019/01/01','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (93763,1,'2017/10/11','2018/09/13','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (93763,2,'2018/04/22','2018/10/01','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (67115,1,'2017/04/30','2019/01/02','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (67115,2,'2018/01/10','2018/09/14','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (40695,1,'2017/05/01','2019/02/18','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (40695,2,'2018/03/01','2018/09/04','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (36488,1,'2017/11/01','2019/04/09','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (36488,2,'2017/08/23','2018/06/21','Genap','Tutup');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (89029,1,'2017/11/23','2018/10/01','Ganjil','Buka');
INSERT INTO SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status) VALUES (89029,2,'2017/06/10','2018/06/08','Genap','Tutup');

INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,21455,'436-65-9829','2017/05/24','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,21455,'459-93-1211','2018/01/28','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,89379,'231-38-8932','2018/01/03','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,89379,'577-33-2818','2017/06/21','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,61014,'607-78-0457','2018/05/14','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,61014,'501-08-9298','2018/05/16','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,19348,'137-04-1781','2018/03/05','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,19348,'610-93-1211','2017/08/26','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,71267,'640-47-6234','2017/07/29','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,71267,'218-76-5590','2018/04/03','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,11930,'272-49-4540','2018/03/10','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,11930,'366-30-2917','2017/08/18','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,10264,'764-52-0172','2017/06/09','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,10264,'112-38-7004','2017/08/22','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,82792,'800-49-7477','2018/04/16','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,82792,'540-26-1905','2017/11/17','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,46796,'524-86-2895','2017/11/25','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,46796,'575-02-1762','2017/12/09','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,92352,'758-88-1894','2017/11/02','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,92352,'860-88-1628','2017/09/19','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,45027,'173-84-3529','2017/11/05','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,45027,'139-59-2179','2018/02/03','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,26732,'487-04-2065','2017/10/25','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,26732,'581-50-4216','2017/10/31','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,65038,'750-73-0354','2017/08/05','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,65038,'772-21-4355','2018/04/12','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,74390,'334-92-9801','2017/07/14','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,74390,'678-53-8288','2018/05/28','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,29252,'719-95-9060','2018/04/11','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,29252,'478-21-9115','2018/01/24','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,48581,'767-13-7088','2018/01/14','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,48581,'183-15-0861','2017/11/19','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,40120,'799-51-2795','2018/04/20','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,40120,'805-66-5681','2017/09/29','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,58544,'347-13-4337','2018/01/26','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,58544,'299-81-0637','2017/10/01','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,13623,'167-48-4916','2018/05/02','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,13623,'267-29-6700','2017/06/26','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,62562,'250-73-7377','2017/06/13','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,62562,'643-33-6215','2017/06/29','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,75345,'217-34-1149','2017/10/21','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,75345,'450-77-5202','2018/01/31','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,63151,'645-89-5615','2017/10/07','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,63151,'112-41-4005','2017/07/22','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,71816,'162-66-4042','2017/05/22','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,71816,'277-94-0551','2018/01/20','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,65306,'896-04-4806','2017/10/20','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,65306,'152-62-1120','2018/04/23','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,94632,'708-53-7033','2017/07/10','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,94632,'811-38-8670','2017/10/19','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,61125,'772-56-1682','2018/02/20','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,61125,'270-94-3271','2017/11/23','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,83025,'531-31-6841','2017/10/27','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,83025,'330-72-1244','2018/02/13','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,96731,'739-78-3341','2018/01/07','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,96731,'872-77-5202','2017/11/06','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,17216,'192-93-2015','2017/07/16','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,17216,'354-75-1644','2017/09/04','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,73760,'360-09-7947','2018/03/27','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,73760,'260-56-7555','2018/05/25','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,95881,'126-86-8003','2018/04/14','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,95881,'300-93-0862','2018/05/27','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,58496,'604-12-4149','2017/04/29','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,58496,'403-79-8569','2018/01/04','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,43913,'305-15-6958','2017/08/03','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,43913,'820-51-6377','2017/06/05','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,58279,'166-49-6849','2017/04/29','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,58279,'154-34-4720','2018/03/02','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,68838,'457-97-6704','2017/10/30','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,68838,'315-96-8326','2017/07/03','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,83983,'520-97-1804','2017/10/17','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,83983,'513-38-4985','2017/04/30','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,70890,'494-03-2037','2017/12/22','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,70890,'192-74-3706','2017/10/09','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,63971,'407-82-6646','2017/09/24','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,63971,'205-96-9725','2018/01/01','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,89026,'101-96-9317','2018/02/03','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,89026,'529-31-4852','2018/01/07','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,72694,'149-21-8324','2018/03/01','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,72694,'171-62-0674','2017/05/06','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,45205,'130-08-2176','2017/12/17','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,45205,'400-13-5516','2018/01/30','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,79608,'604-11-0062','2017/11/26','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,79608,'429-17-3913','2017/09/12','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,33951,'180-27-7430','2017/09/24','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,33951,'825-75-6353','2018/04/21','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,84399,'754-19-0412','2017/12/03','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,84399,'381-27-6538','2017/07/17','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,36096,'157-34-2514','2018/03/17','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,36096,'323-82-8530','2017/07/04','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,93763,'625-32-9303','2018/03/07','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,93763,'482-60-9059','2018/02/10','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,67115,'195-19-9781','2017/12/14','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,67115,'778-65-4685','2017/11/10','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,40695,'156-18-6654','2017/09/19','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,40695,'130-80-0636','2018/04/19','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,36488,'708-76-7859','2017/07/09','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,36488,'743-85-2548','2017/04/30','false','false');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (1,89029,'114-41-3248','2017/11/22','true','true');
INSERT INTO PENDAFTARAN(no_urut,kode_skema_beasiswa,npm,waktu_daftar,status_daftar,status_terima) VALUES (2,89029,'840-49-5137','2017/11/08','false','false');

INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (1,21455,1,'494-03-2037','keterangan 1','2018/05/22',45116);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (2,21455,2,'643-33-6215','keterangan 3','2018/08/27',41700);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (3,89379,1,'778-65-4685','keterangan 1','2018/05/29',30555);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (4,89379,2,'526-37-2889','keterangan 1','2018/11/17',47514);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (5,61014,1,'758-88-1894','keterangan 2','2018/12/09',46095);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (6,61014,2,'811-38-8670','keterangan 3','2018/10/27',35951);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (7,19348,1,'640-47-6234','keterangan 3','2018/10/02',37124);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (8,19348,2,'302-83-4871','keterangan 1','2018/11/22',37836);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (9,71267,1,'661-41-1910','keterangan 2','2018/05/17',48541);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (10,71267,2,'267-29-6700','keterangan 1','2019/01/22',42993);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (11,11930,1,'772-56-1682','keterangan 2','2018/05/06',44957);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (12,11930,2,'277-94-0551','keterangan 2','2018/04/29',40955);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (13,10264,1,'192-74-3338','keterangan 1','2018/06/24',49229);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (14,10264,2,'719-95-9060','keterangan 2','2018/07/08',34666);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (15,82792,1,'482-60-9059','keterangan 2','2018/09/11',47917);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (16,82792,2,'588-08-1782','keterangan 2','2018/07/07',39015);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (17,46796,1,'575-02-1762','keterangan 3','2019/03/03',46964);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (18,46796,2,'254-24-5223','keterangan 3','2018/06/04',45448);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (19,92352,1,'674-95-6828','keterangan 1','2018/09/26',33143);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (20,92352,2,'674-95-6828','keterangan 2','2018/08/19',47677);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (21,45027,1,'135-29-4090','keterangan 3','2018/10/04',49845);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (22,45027,2,'366-30-2917','keterangan 3','2018/05/04',34965);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (23,26732,1,'737-03-5339','keterangan 1','2018/08/24',35351);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (24,26732,2,'500-61-0847','keterangan 2','2019/01/17',39866);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (25,65038,1,'778-72-4710','keterangan 2','2019/04/22',43041);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (26,65038,2,'195-19-9781','keterangan 1','2019/03/27',36164);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (27,74390,1,'626-87-7329','keterangan 2','2018/11/12',35312);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (28,74390,2,'354-75-1644','keterangan 2','2019/03/13',34508);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (29,29252,1,'436-65-9829','keterangan 3','2018/06/21',49219);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (30,29252,2,'173-84-3529','keterangan 1','2018/07/30',30855);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (31,48581,1,'825-82-5294','keterangan 3','2018/07/14',47981);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (32,48581,2,'800-49-7477','keterangan 3','2018/08/01',44381);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (33,40120,1,'459-93-1211','keterangan 2','2018/08/22',38112);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (34,40120,2,'513-38-4985','keterangan 3','2018/12/14',37421);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (35,58544,1,'531-31-6841','keterangan 3','2018/06/10',48534);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (36,58544,2,'152-62-1120','keterangan 2','2018/08/25',30927);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (37,13623,1,'540-26-1905','keterangan 3','2018/08/25',34274);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (38,13623,2,'420-42-1229','keterangan 3','2018/05/22',40619);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (39,62562,1,'250-73-7377','keterangan 1','2018/05/16',31305);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (40,62562,2,'828-17-7829','keterangan 1','2018/05/15',34438);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (41,75345,1,'626-87-7329','keterangan 2','2018/07/21',30712);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (42,75345,2,'272-49-4540','keterangan 2','2019/02/04',44521);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (43,63151,1,'432-28-1751','keterangan 1','2018/08/13',46631);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (44,63151,2,'805-66-5681','keterangan 3','2018/05/04',43069);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (45,71816,1,'698-55-9039','keterangan 1','2019/01/02',37935);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (46,71816,2,'840-49-5137','keterangan 2','2018/11/09',46735);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (47,65306,1,'574-45-9858','keterangan 1','2018/11/28',39456);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (48,65306,2,'615-35-6599','keterangan 2','2018/05/22',32568);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (49,94632,1,'270-94-3271','keterangan 1','2019/01/04',49504);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (50,94632,2,'683-01-5362','keterangan 2','2019/01/19',35577);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (51,61125,1,'366-30-2917','keterangan 3','2018/07/04',47854);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (52,61125,2,'334-26-0597','keterangan 1','2018/08/19',43518);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (53,83025,1,'610-93-1211','keterangan 2','2019/03/28',39624);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (54,83025,2,'299-81-0637','keterangan 1','2018/09/28',43780);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (55,96731,1,'589-80-1119','keterangan 3','2018/12/06',32887);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (56,96731,2,'287-79-1912','keterangan 1','2018/08/08',35271);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (57,17216,1,'366-30-2917','keterangan 1','2018/12/11',39918);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (58,17216,2,'450-77-5202','keterangan 2','2018/12/13',41000);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (59,73760,1,'767-13-7088','keterangan 2','2019/02/24',41955);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (60,73760,2,'302-83-4871','keterangan 3','2019/03/19',39130);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (61,95881,1,'453-42-1885','keterangan 1','2018/06/01',47404);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (62,95881,2,'260-56-7555','keterangan 2','2018/08/18',37488);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (63,58496,1,'758-88-1894','keterangan 2','2018/09/21',39748);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (64,58496,2,'708-53-7033','keterangan 2','2019/03/19',46440);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (65,43913,1,'643-33-6215','keterangan 1','2018/06/11',36849);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (66,43913,2,'195-77-5893','keterangan 2','2019/04/15',48000);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (67,58279,1,'130-80-0636','keterangan 2','2018/08/24',38698);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (68,58279,2,'385-59-9204','keterangan 3','2018/10/02',35441);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (69,68838,1,'539-61-9008','keterangan 2','2019/04/12',49433);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (70,68838,2,'277-94-0551','keterangan 2','2019/02/21',36509);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (71,83983,1,'754-19-0412','keterangan 3','2018/12/19',38971);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (72,83983,2,'604-11-0062','keterangan 3','2018/06/13',42674);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (73,70890,1,'643-33-6215','keterangan 3','2019/01/31',33790);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (74,70890,2,'231-67-9564','keterangan 3','2018/09/09',43746);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (75,63971,1,'459-93-1211','keterangan 3','2018/08/20',35953);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (76,63971,2,'360-09-7947','keterangan 2','2018/10/17',34495);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (77,89026,1,'205-96-9725','keterangan 3','2018/07/12',43652);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (78,89026,2,'482-60-9059','keterangan 2','2018/10/22',33800);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (79,72694,1,'157-34-2514','keterangan 1','2018/11/14',42670);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (80,72694,2,'116-86-8009','keterangan 3','2019/03/02',36637);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (81,45205,1,'524-86-2895','keterangan 2','2018/05/18',46299);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (82,45205,2,'778-65-4685','keterangan 2','2018/08/19',36345);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (83,79608,1,'607-78-0457','keterangan 1','2018/12/17',47766);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (84,79608,2,'401-78-6216','keterangan 1','2018/10/02',47843);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (85,33951,1,'743-12-5145','keterangan 3','2019/01/30',36517);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (86,33951,2,'575-26-9464','keterangan 3','2018/06/24',36006);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (87,84399,1,'130-08-2176','keterangan 2','2018/07/15',36098);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (88,84399,2,'588-08-1782','keterangan 1','2019/02/16',45863);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (89,36096,1,'442-50-0642','keterangan 1','2019/04/03',38406);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (90,36096,2,'136-59-1434','keterangan 1','2018/08/11',33504);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (91,93763,1,'800-49-7477','keterangan 3','2018/12/05',40860);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (92,93763,2,'420-11-1467','keterangan 1','2019/03/12',42082);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (93,67115,1,'674-95-6828','keterangan 1','2019/03/30',42236);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (94,67115,2,'626-87-7329','keterangan 3','2018/12/13',35797);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (95,40695,1,'302-83-4871','keterangan 2','2018/05/02',45758);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (96,40695,2,'607-78-0457','keterangan 3','2019/03/22',39201);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (97,36488,1,'600-87-7392','keterangan 2','2018/08/29',43146);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (98,36488,2,'825-82-5294','keterangan 1','2018/12/05',40095);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (99,89029,1,'149-21-8324','keterangan 1','2018/07/01',36817);
INSERT INTO PEMBAYARAN(urutan,kode_skema_beasiswa,no_urut_skema_beasiswa_aktif,npm,keterangan,tgl_bayar,nominal) VALUES (100,89029,2,'166-49-6849','keterangan 2','2019/01/30',42940);
	
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (903,'Cambridge','Zhaobaoshan');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (148,'Shoshone','Aguaí');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (778,'Darwin','Lazaro Cardenas');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (921,'Summit','Brody');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (294,'Johnson','Si Sa Ket');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (224,'Ridgeway','Boulsa');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (672,'Village','Doumé');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (252,'Gerald','Suwaru');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (320,'Manufacturers','Besuki');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (607,'2nd','Lemende');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (727,'Upham','Hovtamej');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (962,'Prentice','Obodivka');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (689,'Portage','Koblain');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (919,'Mccormick','Qal‘ah-ye Kūf');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (842,'Golf','Marataizes');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (527,'Stuart','Vrede');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (402,'Ryan','Heshang');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (906,'Mccormick','Sơn Tây');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (192,'Warrior','Fengqiao');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (560,'Tennessee','Lazaro Cardenas');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (849,'Del Mar','Nora');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (387,'Dunning','Aviles');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (452,'Spenser','Pasar');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (791,'Onsgard','Sadar Bazar');
INSERT INTO TEMPAT_WAWANCARA(kode,nama,lokasi) VALUES (645,'Charing Cross','Baoxia');

INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,17216,'2018-11-10 04:03:58',452);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,36096,'2018-09-19 12:28:36',560);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,19348,'2019-01-17 05:59:59',962);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,45205,'2018-10-23 06:13:44',294);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,70890,'2018-09-24 11:25:12',148);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,65038,'2018-09-13 07:54:01',387);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,29252,'2019-03-10 17:04:36',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,61014,'2018-09-25 23:04:51',224);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,67115,'2018-07-27 08:06:19',224);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,89029,'2018-05-20 10:38:04',919);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,73760,'2018-06-19 03:51:23',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,58279,'2018-05-14 18:19:59',645);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,58544,'2019-03-04 18:59:06',148);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,43913,'2018-10-30 22:07:40',148);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,89026,'2018-07-27 03:47:35',402);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,93763,'2018-12-05 21:24:07',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,17216,'2018-06-07 08:33:20',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,62562,'2018-11-25 22:13:50',791);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,83025,'2018-12-20 13:57:47',919);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,75345,'2018-10-24 12:46:24',645);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,33951,'2019-01-05 19:37:30',294);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,96731,'2019-03-08 02:43:20',224);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,75345,'2019-04-01 13:31:33',689);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,63151,'2019-02-17 11:22:36',672);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,74390,'2018-08-17 20:10:20',148);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,63971,'2019-03-20 07:17:29',252);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,92352,'2019-01-31 02:40:54',452);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,61125,'2018-07-06 02:40:29',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,36096,'2019-04-02 22:19:19',452);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,83025,'2018-08-17 17:59:48',402);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,36096,'2019-04-02 02:58:46',560);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,89379,'2018-09-25 03:55:36',452);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,92352,'2018-05-15 09:00:59',849);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,63971,'2018-08-22 03:57:02',148);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,29252,'2019-04-10 00:22:25',527);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,74390,'2018-04-30 01:13:03',727);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,58496,'2018-09-21 17:18:20',645);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,71267,'2018-12-06 14:47:29',294);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,36488,'2018-05-16 12:29:18',402);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,84399,'2019-04-03 09:12:40',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,89029,'2018-11-21 19:36:32',778);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,19348,'2019-04-06 19:07:54',560);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,21455,'2019-01-07 15:29:05',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,65306,'2018-08-31 18:42:14',252);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,11930,'2018-11-15 06:06:38',778);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,63151,'2019-02-04 16:28:51',252);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,89026,'2018-08-28 10:13:17',727);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,45027,'2019-04-04 23:21:02',645);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,70890,'2019-04-26 16:24:10',252);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,89029,'2019-01-26 18:44:49',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,94632,'2018-07-26 00:52:29',148);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,43913,'2019-03-18 09:09:23',527);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,75345,'2018-12-08 00:49:54',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,67115,'2019-01-23 15:20:55',962);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,83983,'2019-01-10 08:26:06',672);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,72694,'2018-10-29 21:56:25',672);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,61125,'2019-01-28 22:15:55',791);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,65038,'2018-09-27 00:54:20',962);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,68838,'2019-01-10 13:52:40',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,61125,'2018-06-28 11:56:03',903);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,36488,'2019-01-20 10:38:34',672);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,93763,'2018-06-28 11:35:05',607);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,61125,'2018-12-15 00:25:11',527);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,68838,'2018-07-04 00:05:15',252);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,21455,'2018-05-23 17:14:08',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,67115,'2019-02-06 09:16:46',645);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,82792,'2019-01-21 09:36:22',402);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,65038,'2018-10-08 11:06:28',527);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,61014,'2018-11-28 08:13:27',962);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,62562,'2018-09-04 19:52:04',689);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,29252,'2019-04-28 19:25:22',402);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,19348,'2019-02-17 12:20:45',791);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,84399,'2019-01-14 22:55:47',689);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,84399,'2018-07-26 07:04:42',778);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,19348,'2018-10-31 01:19:12',842);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,75345,'2018-07-12 19:15:43',224);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,89026,'2018-07-27 16:43:13',294);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,96731,'2018-05-04 03:29:28',791);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,68838,'2018-12-08 11:33:09',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,33951,'2018-09-02 07:07:34',402);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,45027,'2018-07-09 18:14:37',224);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,46796,'2019-01-20 09:32:01',607);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,11930,'2018-06-19 14:10:10',921);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,96731,'2019-04-19 19:13:35',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,75345,'2018-07-15 20:39:11',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,84399,'2019-02-08 21:41:56',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,63971,'2019-03-06 09:49:50',148);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,73760,'2018-09-15 08:19:20',560);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,65306,'2019-02-13 03:42:44',452);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,94632,'2018-11-10 03:20:43',560);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,79608,'2019-03-20 12:40:51',387);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,11930,'2019-02-02 05:19:26',402);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,45027,'2018-10-05 21:09:46',527);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,68838,'2018-10-18 21:03:04',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,62562,'2018-07-31 05:08:21',672);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,61125,'2019-04-02 21:45:25',527);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,83025,'2018-11-17 17:13:35',192);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (2,40120,'2019-04-01 17:18:05',906);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,17216,'2018-12-20 13:59:23',778);
INSERT INTO WAWANCARA(no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,jadwal,kode_tempat_wawancara) VALUES (1,61014,'2019-02-18 23:30:05',778);

INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/08/02',1,40695,'mdrewry10','PERIHAL BEASISWA','Pellentesque viverra pede ac diam.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/02/28',1,68838,'jlanfareq','PENTING!!!!','Cras pellentesque volutpat dui.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/10/03',2,84399,'lpaueru','PERIHAL BEASISWA','Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/03/20',2,72694,'keckford17','PENTING!!!!','Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/02/10',2,93763,'ayousonw','PERIHAL BEASISWA','In congue. Etiam justo.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/02/06',2,10264,'keckford17','PENGUMUMAN','Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/11/01',1,29252,'kbrachell8','PENGUMUMAN','Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/07/23',1,71816,'rbispham1','PENGUMUMAN','Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/03/17',2,95881,'crawlev','PENTING!!!!','Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/05',1,40120,'dlansly1d','PENGUMUMAN','Phasellus id sapien in sapien iaculis congue.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/07/17',1,89029,'hbarley1b','PENTING!!!!','Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/09/30',1,19348,'clavrinovg','PENGUMUMAN','Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/11/22',2,72694,'gabysr','PERIHAL BEASISWA','Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/01/31',1,40695,'evignaux7','PENTING!!!!','Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/01/31',2,33951,'gabysr','PENTING!!!!','Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/08/16',2,48581,'pattenbrowh','PERIHAL BEASISWA','Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/31',1,58496,'rchamney5','PENGUMUMAN','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/08/11',2,75345,'rbispham1','PERIHAL BEASISWA','Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/07/23',2,72694,'clavrinovg','PERIHAL BEASISWA','Vestibulum sed magna at nunc commodo placerat. Praesent blandit.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/04/01',2,13623,'pbardayn','PENTING!!!!','Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/12/01',2,36488,'smithonb','PENGUMUMAN','In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/15',1,58279,'evignaux7','PERIHAL BEASISWA','Aliquam quis turpis eget elit sodales scelerisque.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/02/01',1,10264,'mcheatlek','PENGUMUMAN','Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/12/10',1,94632,'wslyman9','PENGUMUMAN','Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/12/24',2,46796,'agullberg11','PENGUMUMAN','Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/10/17',1,62562,'dslye','PENGUMUMAN','Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/12/05',1,74390,'vfegany','PENTING!!!!','Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/04/06',1,40695,'vfegany','PERIHAL BEASISWA','Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/06/20',2,61014,'crawlev','PENTING!!!!','In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/09/06',1,89029,'dlansly1d','PENTING!!!!','Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/26',1,61125,'orawless','PENGUMUMAN','Nam nulla.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/09/30',2,94632,'gabysr','PENGUMUMAN','Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/01/27',1,45027,'sbambridge16','PENTING!!!!','Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/09/03',2,26732,'uallixl','PENTING!!!!','Vivamus tortor.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/11/30',1,40120,'rjopson19','PERIHAL BEASISWA','Integer a nibh. In quis justo.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/26',2,58544,'gcopod','PENTING!!!!','Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/01/19',1,11930,'ajanecekt','PENGUMUMAN','Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/11',1,74390,'gcopod','PERIHAL BEASISWA','Cras non velit nec nisi vulputate nonummy.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/16',2,82792,'clavrinovg','PENGUMUMAN','Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/05',1,58544,'nlortzingz','PENTING!!!!','Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/05/01',1,36488,'kwoodcraft2','PENGUMUMAN','Praesent lectus.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/07/23',2,89026,'agullberg11','PENTING!!!!','Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/09/24',1,46796,'uallixl','PENTING!!!!','Aliquam sit amet diam in magna bibendum imperdiet.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/12/25',2,89029,'dlansly1d','PERIHAL BEASISWA','Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/09/15',1,70890,'kkytei','PENGUMUMAN','Praesent blandit. Nam nulla.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2017/12/31',2,75345,'keckford17','PENTING!!!!','Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/03/10',2,89379,'hkirvell13','PENTING!!!!','Aenean lectus.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/03/07',1,36096,'rchamney5','PENTING!!!!','Nulla tellus. In sagittis dui vel nisl.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/03/25',1,83983,'nlortzingz','PENTING!!!!','Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.');
INSERT INTO PENGUMUMAN(tanggal,no_urut_skema_beasiswa_aktif,kode_skema_beasiswa,username,judul,isi) VALUES ('2018/01/01',1,82792,'wslyman9','PENTING!!!!','Duis at velit eu est congue elementum.');
