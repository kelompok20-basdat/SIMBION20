from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^registrasi-mahaisiswa/', registrasi_mahasiswa, name='registrasi_mahasiswa'),
    url(r'^registrasi-donatur-individual/', registrasi_donatur_individual, name='registrasi_donatur_individual'),
    url(r'^registrasi-donatur-yayasan/', registrasi_donatur_yayasan, name='registrasi_donatur_yayasan'),
    url(r'^registrasi-validate-username/', validate_username, name='validate_username'),
    url(r'^registrasi-validate-no-identitas/', validate_no_identitas, name='validate_no_identitas'),
    url(r'^registrasi-validate-mahasiswa/', validate_mahasiswa, name='validate_mahasiswa'),
    url(r'^registrasi-validate-individu/', validate_individu, name='validate_individu'),
    url(r'^registrasi-validate-yayasan/', validate_yayasan, name='validate_yayasan')
]