from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from halaman_utama.models import *
from halaman_utama.views import dictfetchall

cursor = connection.cursor()

# Create your views here.
def index(request):
    response = {}
    return render(request, 'register/Registrasi_umum.html', response)

@csrf_exempt
def validate_username(request):
	username = request.POST.get('username', None)
	print(username)
	cursor.execute('''	SELECT *
						FROM pengguna
						WHERE username = %s''', [username])
	catch = cursor.fetchall()
	data = {
		'is_taken': (len(catch) > 0)  
	}
	return JsonResponse(data)

@csrf_exempt
def validate_mahasiswa(request):
	username = request.POST.get('username', None)
	npm = request.POST.get('npm', None)
	print(username)
	cursor.execute('''	SELECT *
						FROM pengguna
						WHERE username = %s''', [username])
	catch_username = cursor.fetchall()
	cursor.execute('''	SELECT *
						FROM mahasiswa
						WHERE npm = %s''', [npm])
	catch_npm = cursor.fetchall()
	data = {
		'username_is_taken': (len(catch_username) > 0),
		'npm_is_taken' : (len(catch_npm) > 0)
	}
	return JsonResponse(data)

@csrf_exempt
def validate_no_identitas(request):
	nomor_identitas = request.POST.get('nomor_identitas', None)
	cursor.execute('''	SELECT *
						 	FROM donatur
						 	WHERE nomor_identitas = %s''', [nomor_identitas])
	catch = cursor.fetchall()
	data = {
		'is_taken': (len(catch) > 0)  
	}
	return JsonResponse(data)

@csrf_exempt
def validate_individu(request):
	username = request.POST.get('username', None)
	nik = request.POST.get('nik', None)
	print(username)
	cursor.execute('''	SELECT *
						FROM pengguna
						WHERE username = %s''', [username])
	catch_username = cursor.fetchall()
	cursor.execute('''	SELECT *
						FROM individual_donor
						WHERE nik = %s''', [nik])
	catch_nik = cursor.fetchall()
	data = {
		'username_is_taken': (len(catch_username) > 0),
		'nik_is_taken' : (len(catch_nik) > 0),
		'length_is_not' : (len(nik) != 16)
	}
	return JsonResponse(data)

@csrf_exempt
def validate_yayasan(request):
	username = request.POST.get('username', None)
	sk = request.POST.get('sk', None)
	print(username)
	cursor.execute('''	SELECT *
						FROM pengguna
						WHERE username = %s''', [username])
	catch_username = cursor.fetchall()
	cursor.execute('''	SELECT *
						FROM yayasan
						WHERE no_sk_yayasan = %s''', [sk])
	catch_sk = cursor.fetchall()
	data = {
		'username_is_taken': (len(catch_username) > 0),
		'sk_is_taken' : (len(catch_sk) > 0)
	}
	return JsonResponse(data)

def registrasi_mahasiswa(request):
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		npm = request.POST['npm']
		email = request.POST['email']
		nama = request.POST['nama-lengkap']
		if (request.POST['no-telepon'] == ""):
			no_telp = ""
		else :	
			no_telp = request.POST['no-telepon']
		tempat_tinggal = request.POST['alamat-tempat-tinggal']
		domisili = request.POST['alamat-domisili']
		bank = request.POST['instansi-bank']
		norek = request.POST['no-rekening']
		pemilik = request.POST['nama-pemilik']
		print(username)
		print(password)
		cursor.execute('''	INSERT INTO PENGGUNA(username,password,role) 
							VALUES ( %s, %s,'mahasiswa');''', [username, password])
		cursor.execute('''	INSERT INTO MAHASISWA(npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) 
							VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);''', 
							[npm, email, nama, no_telp, tempat_tinggal, domisili, bank, norek, pemilik, username])
		response = {}
		return HttpResponseRedirect(reverse('login:login'))

# JANGAN LUPA KERJAIN YANG INI 
def registrasi_donatur_individual(request):
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		no_identitas = request.POST['no-identitas']
		nik = request.POST['nik']
		email = request.POST['email']
		nama = request.POST['nama-lengkap']
		npwp = request.POST['npwp']
		no_telp = request.POST['no-telepon']
		alamat = request.POST['alamat-lengkap']
		print(username)
		print(password)
		cursor.execute('''	INSERT INTO PENGGUNA(username,password,role) 
							VALUES ( %s, %s,'donatur');''', [username, password])
		cursor.execute('''	INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) 
							VALUES (%s, %s, %s, %s, %s, %s, %s);''',
							 [no_identitas, email, nama, npwp, no_telp, alamat, username])
		cursor.execute('''	INSERT INTO INDIVIDUAL_DONOR(nik,nomor_identitas_donatur)
							VALUES (%s, %s);''', [nik, no_identitas])
		response = {}
		return HttpResponseRedirect(reverse('login:login'))

def registrasi_donatur_yayasan(request):
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		no_identitas = request.POST['no-identitas']
		no_sk_yayasan = request.POST['no-sk-yayasan']
		email = request.POST['email']
		nama = request.POST['nama-yayasan']
		npwp = request.POST['npwp']
		no_telp_cp = request.POST['no-telepon-cp']
		alamat = request.POST['alamat-lengkap']
		print(username)
		print(password)
		cursor.execute('''	INSERT INTO PENGGUNA(username,password,role) 
							VALUES ( %s, %s,'donatur');''', [username, password])
		cursor.execute('''	INSERT INTO DONATUR(nomor_identitas,email,nama,npwp,no_telp,alamat,username) 
							VALUES (%s, %s, %s, %s, %s, %s, %s);''', 
							[no_identitas, email, nama, npwp, no_telp_cp, alamat, username])
		cursor.execute('''	INSERT INTO YAYASAN(no_sk_yayasan,email,nama,no_telp_cp,nomor_identitas_donatur) 
							VALUES (%s, %s, %s, %s, %s);''', 
							[no_sk_yayasan, email, nama, no_telp_cp, no_identitas])		
		response = {}
		return HttpResponseRedirect(reverse('halaman_utama:index'))		